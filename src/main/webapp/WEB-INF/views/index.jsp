<!DOCTYPE html>
<html ng-app="jeff.app">
	<head >

	</head>

	<body>
		<!-- header -->
		<div ui-view="header"></div>

		<!-- main -->
		<div ui-view="content"></div>

		<!-- footer -->
		<div ui-view="footer"></div>

		<script src="webjars/angularjs/1.4.8/angular.js"></script>
		<script src="webjars/angular-ui-router/0.2.18/angular-ui-router.min.js"></script>

		<!-- main app -->
		<script src="app/js/app.js"></script>
		<script src="app/js/app-route.js"></script>
		<script src="app/js/app-service.js"></script>

		<!-- route folder -->
		<script src="app/js/route/author-route.js"></script>
		<script src="app/js/route/login-route.js"></script>
		<script src="app/js/route/user-route.js"></script>
		<script src="app/js/route/book-route.js"></script>
		<script src="app/js/route/category-route.js"></script>
		<script src="app/js/route/publisher-route.js"></script>
		<script src="app/js/route/shelf-route.js"></script>
		<script src="app/js/route/stock-route.js"></script>
		<script src="app/js/route/approval-route.js"></script>
		<script src="app/js/route/cart-route.js"></script>
		<script src="app/js/route/contract-route.js"></script>
		<script src="app/js/route/update-user-route.js"></script>

		<!-- service folder -->
		<script src="app/js/service/author-service.js"></script>
		<script src="app/js/service/user-service.js"></script>
		<script src="app/js/service/login-service.js"></script>
		<script src="app/js/service/category-service.js"></script>
		<script src="app/js/service/publisher-service.js"></script>
		<script src="app/js/service/book-service.js"></script>
		<script src="app/js/service/cart-service.js"></script>
		<script src="app/js/service/contract-service.js"></script>
		<script src="app/js/service/shelf-service.js"></script>

		<!-- controller folder -->
		<script src="app/js/controller/header-ctrl.js"></script>
		<script src="app/js/controller/footer-ctrl.js"></script>
		<script src="app/js/controller/author-ctrl.js"></script>
		<script src="app/js/controller/user-ctrl.js"></script>
		<script src="app/js/controller/login-ctrl.js"></script>
		<script src="app/js/controller/category-ctrl.js"></script>
		<script src="app/js/controller/publisher-ctrl.js"></script>
		<script src="app/js/controller/book-ctrl.js"></script>
		<script src="app/js/controller/book-user-ctrl.js"></script>
		<script src="app/js/controller/contract-ctrl.js"></script>
		<script src="app/js/controller/approval-ctrl.js"></script>
		<script src="app/js/controller/update-user-ctrl.js"></script>
		<script src="app/js/controller/shelf-ctrl.js"></script>

		<!-- lib -->
		<script src="app/js/lib/uniqueSignature.js"></script>
	</body>
</html>


<!-- //router.js
var app = angular.module('myApp',['ui-router'])
		.config(['$urlRouterProvider','$stateProvider',function($urlRouterProvider,$stateProvider)]) {
			$urlRouterProvider.otherwise('/');
			$stateProvider
				.state('home', {
					url:'/',
					templateUrl: 'app/views/home.html',
					controller: 'homeCtrl'
					resolve: {
						friends: ['$http', function($http) {
							return $http.get('/dsad/assad/test.json').then(function(response) {
								return response.data;
							})
						}]
					}
				})


		})

//ctrl
angular
.module('myApp')
.controller('homeCtrl',['$scope', function($scope) {
	$scope.title = "";
	$scope.items = ['','',''];
	$scope.save = function() {
		$http.post('/dsad/assad',friends);
	};

	$scope.delete = function() {
		$http.post('/dsad/assad',friends);
	};
}]); -->