'use strict';
angular
.module('jeff.service.app')
.factory('LoginService', ['$http', '$log', LoginService]);

function LoginService($http, $log) {
	var ret = {};

	ret.getUserId = function (username, pw) {
		return $http({
			method : 'GET',
			url : 'http://localhost:8080/leon-spring-assignment/user-login/',
			params : {username : username, pw : pw}
		});
	};

	ret.getModeratorId = function (username, pw) {
		return $http({
			method : 'GET',
			url : 'http://localhost:8080/leon-spring-assignment/moderator-login/',
			params : {username : username, pw : pw}
		});
	};

	return ret;
}