'use strict';
angular
.module('jeff.service.app')
.factory('CategoryService', ['$http', '$q', function($http, $q){
 
    return {
            findAllCategory: function() {
                    return $http.get('http://localhost:8080/leon-spring-assignment/category-moderator/')
                            .then(
                                    function(response){
                                        return response.data;
                                    }, 
                                    function(errResponse){
                                        console.error('Error while fetching categories');
                                        return $q.reject(errResponse);
                                    }
                            );
            },
             
            addCategory: function(category){
                    return $http.post('http://localhost:8080/leon-spring-assignment/category-moderator/', category)
                            .then(
                                    function(response){
                                        return response.data;
                                    }, 
                                    function(errResponse){
                                        console.error('Error while creating category');
                                        return $q.reject(errResponse);
                                    }
                            );
            },
             
            updateCategory: function(category, categoryId){
                    return $http.put('http://localhost:8080/leon-spring-assignment/category-moderator/'+categoryId, category)
                            .then(
                                    function(response){
                                        return response.data;
                                    }, 
                                    function(errResponse){
                                        console.error('Error while updating category');
                                        return $q.reject(errResponse);
                                    }
                            );
            },
             
            deleteCategory: function(categoryId){
                    return $http.delete('http://localhost:8080/leon-spring-assignment/category-moderator/'+categoryId)
                            .then(
                                    function(response){
                                        return response.data;
                                    }, 
                                    function(errResponse){
                                        console.error('Error while deleting category');
                                        return $q.reject(errResponse);
                                    }
                            );
            }
         
    };
 
}]);