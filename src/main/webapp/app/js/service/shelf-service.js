'use strict';
angular
.module('jeff.service.app')
.factory('ShelfService', ['$http', '$q', function($http, $q){

    return {
            findUserBooks: function(userId) {
                    return $http.get('http://localhost:8080/leon-spring-assignment/shelf-books/' + userId)
                            .then(
                                    function(response){
                                        return response.data;
                                    },
                                    function(errResponse){
                                        console.error('Error while fetching books');
                                        return $q.reject(errResponse);
                                    }
                            );
            },
            accessUserShelf: function(userId) {
                return $http.get('http://localhost:8080/leon-spring-assignment/shelf-user/' + userId)
                        .then(
                                function(response){
                                    return response.data;
                                },
                                function(errResponse){
                                    console.error('Error while fetching books');
                                    return $q.reject(errResponse);
                                }
                        );
        }
    };

}]);