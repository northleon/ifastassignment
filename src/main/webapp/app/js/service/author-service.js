'use strict';
angular
.module('jeff.service.app')
.factory('AuthorService', ['$http', '$q', function($http, $q){
 
    return {
            findAllAuthor: function() {
                    return $http.get('http://localhost:8080/leon-spring-assignment/author-moderator/')
                            .then(
                                    function(response){
                                        return response.data;
                                    }, 
                                    function(errResponse){
                                        console.error('Error while fetching authors');
                                        return $q.reject(errResponse);
                                    }
                            );
            },
             
            addAuthor: function(author){
                    return $http.post('http://localhost:8080/leon-spring-assignment/author-moderator/', author)
                            .then(
                                    function(response){
                                        return response.data;
                                    }, 
                                    function(errResponse){
                                        console.error('Error while creating author');
                                        return $q.reject(errResponse);
                                    }
                            );
            },
             
            updateAuthor: function(author, authorId){
                    return $http.put('http://localhost:8080/leon-spring-assignment/author-moderator/'+authorId, author)
                            .then(
                                    function(response){
                                        return response.data;
                                    }, 
                                    function(errResponse){
                                        console.error('Error while updating author');
                                        return $q.reject(errResponse);
                                    }
                            );
            },
             
            deleteUser: function(authorId){
                    return $http.delete('http://localhost:8080/leon-spring-assignment/author-moderator/'+authorId)
                            .then(
                                    function(response){
                                        return response.data;
                                    }, 
                                    function(errResponse){
                                        console.error('Error while deleting author');
                                        return $q.reject(errResponse);
                                    }
                            );
            }
         
    };
 
}]);