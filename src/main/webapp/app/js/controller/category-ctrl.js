'use strict';
angular
.module('jeff.app')
.controller('CategoryController', ['$scope','CategoryService', function($scope,CategoryService) {
          var self = this;
          self.category={categoryId:null,categoryName:'',categoryDescription:''};
          self.categories=[];

          self.findAllCategory = function(){
              CategoryService.findAllCategory()
                  .then(
                               function(d) {
                                    self.categories = d;
                               },
                                function(errResponse){
                                    console.error('Error while fetching categories');
                                }
                       );
          };

          self.validateCategory = (category) => {
        	  if (category.categoryName.length > 50) {
        		  alert('Category Name too long');
        		  return false;
        	  }

        	  if (category.categoryDescription.length > 100) {
        		  alert('Category Description too long');
        		  return false;
        	  }

        	  return true;
          }

          self.addCategory = function(category){
        	  let valid = self.validateCategory(category);
        	  if (!valid) {
        		  console.log('Category validation failed');
        		  return false;
        	  }
              CategoryService.addCategory(category)
                      .then(
                      self.findAllCategory,
                              function(errResponse){
                                   console.error('Error while creating category.');
                              }
                  );
          };

         self.updateCategory = function(category, categoryId){
        	 CategoryService.updateCategory(category, categoryId)
                      .then(
                              self.findAllCategory,
                              function(errResponse){
                                   console.error('Error while updating category.');
                              }
                  );
          };

         self.deleteCategory = function(categoryId){
        	 CategoryService.deleteCategory(categoryId)
                      .then(
                              self.findAllCategory,
                              function(errResponse){
                                   console.error('Error while deleting category.');
                              }
                  );
          };

          self.findAllCategory();

          self.submit = function() {
              if(self.category.categoryId===null){
                  console.log('Saving New Category', self.category);
                  self.addCategory(self.category);
              }else{
                  self.updateCategory(self.category, self.category.categoryId);
                  console.log('Category updated with categoryId ', self.category.categoryId);
              }
              self.reset();
          };

          self.edit = function(categoryId){
              console.log('categoryId to be edited', categoryId);
              for(var i = 0; i < self.catogeries.length; i++){
                  if(self.catogeries[i].categoryId === categoryId) {
                     self.category = angular.copy(self.catogeries[i]);
                     break;
                  }
              }
          };

          self.remove = function(categoryId){
              console.log('categoryId to be deleted', categoryId);
              if(self.category.categoryId === categoryId) {//clean form if the category to be deleted is shown there.
                 self.reset();
              }
              self.deleteCategory(categoryId);
          };


          self.reset = function(){
        	  self.category={categoryId:null,categoryName:'',categoryDescription:''};
              $scope.formCategory.$setPristine(); //reset Form
          };

      }]);