'use strict';

angular
.module('jeff.app')
.controller('BookController', ['$scope', 'BookService', 'AuthorService','PublisherService','CategoryService',function($scope,BookService,AuthorService,PublisherService,CategoryService) {
          var self = this;
          self.author={authorId:'',authorName:'',authorEmail:'',authorCountry:''};
          self.publisher={publisherId:'',publisherName:'',publisherEmail:'',publisherAddress:'',publisherTelephone:'',publisherFax:''};
          self.category={categoryId:'',categoryName:'',categoryDescription:''};
          self.book={bookId:null,
        		  bookSubject:'',
        		  bookDescription:'',
        		  bookIsbn:'',
        		  author:{authorId:'',authorName:'',authorEmail:'',authorCountry:''},
        		  publisher:{publisherId:'',publisherName:'',publisherEmail:'',publisherAddress:'',publisherTelephone:'',publisherFax:''},
        		  category:{categoryId:'',categoryName:'',categoryDescription:''},
        		  bookContent:'',
        		  bookContentType:'',
        		  bookPrice:''};

          self.books=[];
          self.bookAuthors=[];
          self.bookPublishers=[];
          self.bookCategories=[];

          self.actions=['Add Book','Save Edit'];
          self.action = self.actions[0];

          self.findAllAuthor = function(){
              AuthorService.findAllAuthor()
                  .then(
                               function(d) {
                                    self.bookAuthors = d;
                                    console.log(d)
                                    console.log('bookAuthors '+self.bookAuthors.length);
/*                                    for(var i = 0; i < self.bookAuthors.length; i++) {
                                    	console.log('bookAuthors['+i+'] - START');
                                    	console.log('id - '+self.bookAuthors[i].authorId);
                                    	console.log('name - '+self.bookAuthors[i].authorName);
                                    	console.log('email - '+self.bookAuthors[i].authorEmail);
                                    	console.log('country - '+self.bookAuthors[i].authorEmail);
                                    	console.log('bookAuthors['+i+'] - END');
                                    }*/
                               },
                                function(errResponse){
                                    console.error('Error while fetching authors');
                                }
                       );
          };

          self.findAllPublisher = function(){
        	  PublisherService.findAllPublisher()
                  .then(
                               function(d) {
                                    self.bookPublishers = d;
                                    console.log('bookPublishers '+self.bookPublishers.length);
                               },
                                function(errResponse){
                                    console.error('Error while fetching publishers');
                                }
                       );
          };

          self.findAllCategory = function(){
              CategoryService.findAllCategory()
                  .then(
                               function(d) {
                                    self.bookCategories = d;
                                    console.log('bookCategories '+self.bookCategories.length);
                               },
                                function(errResponse){
                                    console.error('Error while fetching categories');
                                }
                       );
          };

          self.findAllBook = function(){
              BookService.findAllBook()
                  .then(
                               function(d) {
                                    self.books = d;
                                    console.log('books '+self.books.length);
                                    console.log(self.books);
                               },
                                function(errResponse){
                                    console.error('Error while fetching books');
                                }
                       );
          };

          self.validateBook = (book) => {
        	  if (book.bookSubject.length > 20) {
        		  alert('Book Subject too long.');
        		  return false;
        	  }

        	  if (book.bookDescription.length > 20) {
        		  alert('Book Description too long.')
        		  return false;
        	  }

        	  if (book.bookIsbn.length > 20) {
        		  alert('Book ISBN too long');
        		  return false;
        	  }

        	  if (isNaN(book.bookPrice) || Number(book.bookPrice) <= 0){
        		  alert('Invalid Book Price entered');
        		  return false;
        	  }

        	  return true;
          }

          self.addBook = function(book){
        	  self.action = self.actions[0];
        	  let valid = self.validateBook(book);
        	  if (!valid) {
        		  console.log('Book validation failed');
        		  return false;
        	  }

              BookService.addBook(book)
                      .then(
                      self.findAllBook,
                              function(errResponse){
                                   console.error('Error while creating book.');
                              }
                  );
          };

         self.updateBook = function(book, bookId){
        	 self.action = self.actions[0];
   	  		 let valid = self.validateBook(book);
   	  		 if (!valid) {
		  		  console.log('Book validation failed');
		  		  return false;
   	  		 }

        	 BookService.updateBook(book, bookId)
                      .then(
                              self.findAllBook,
                              function(errResponse){
                                   console.error('Error while updating book.');
                              }
                  );
          };

         self.deleteBook = function(bookId){
              BookService.deleteBook(bookId)
                      .then(
                              self.findAllBook,
                              function(errResponse){
                                   console.error('Error while deleting book.');
                              }
                  );
          };

          self.findAllAuthor();
          self.findAllPublisher();
          self.findAllCategory();
          self.findAllBook();

          self.submit = function() {
              if(self.book.bookId===null){
            	  self.bookForm={bookId:self.book.bookId,
                		  bookSubject:self.book.bookSubject,
                		  bookDescription:self.book.bookDescription,
                		  bookIsbn:self.book.bookIsbn,
                		  authorId:self.book.author.authorId,
                		  publisherId:self.book.publisher.publisherId,
                		  categoryId:self.book.category.categoryId,
                		  bookContent:self.book.bookContent,
                		  bookContentType:self.book.bookContentType,
                		  bookPrice:self.book.bookPrice};
                  console.log('Saving New Book', self.bookForm);
                  self.addBook(self.bookForm);
              }else{
                  self.updateBook(self.book, self.book.bookId);
                  console.log('Book updated with bookId ', self.book.bookId);
              }
              self.reset();
          };

          self.edit = function(bookId){
              console.log('bookId to be edited', bookId);
              for(var i = 0; i < self.books.length; i++){
                  if(self.books[i].bookId === bookId) {
                     self.book = angular.copy(self.books[i]);
                     self.action = self.actions[1];
                     break;
                  }
              }
          };

          self.remove = function(bookId){
              console.log('bookId to be deleted', bookId);
              if(self.book.bookId === bookId) {//clean form if the author to be deleted is shown there.
                 self.reset();
              }
              self.deleteBook(bookId);
          };


          self.reset = function(){
        	  self.book={bookId:null,
            		  bookSubject:'',
            		  bookDescription:'',
            		  bookIsbn:'',
            		  author:{authorId:'',authorName:'',authorEmail:'',authorCountry:''},
            		  publisher:{publisherId:'',publisherName:'',publisherEmail:'',publisherAddress:'',publisherTelephone:'',publisherFax:''},
            		  category:{categoryId:'',categoryName:'',categoryDescription:''},
            		  bookContent:'',
            		  bookContentType:'',
            		  bookPrice:''};
              $scope.formBook.$setPristine(); //reset Form
          };

      }]);