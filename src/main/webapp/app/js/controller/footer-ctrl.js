'use strict';
angular
.module('jeff.app')
.controller('FooterController', ['$state','$location', function($state,$location) {
	var self = this;

	self.validate = (value) => (value != 'null' && value != null);

	if(self.validate(window.localStorage['userId'])) {
		self.loginId = window.localStorage['userId'];
		console.log('user');
	}

	else if (self.validate(window.localStorage['moderatorId'])) {
		self.loginId = window.localStorage['moderatorId'];
		console.log('moderator');
	}
	else {
		let ignoreRoute = ['/login','/register'];

		if (!ignoreRoute.includes($location.path())){
			self.loginId = '';
			console.log('default');
			$state.go('book');
		}

	}

}]);