'use strict';

angular
.module('jeff.app')
.controller('ContractController', ['$scope','$state', 'CartService', 'UserService','ContractService','ShelfService',function($scope,$state,CartService,UserService,ContractService,ShelfService) {
          var self = this;
          var totalPrice;
          self.cart = CartService.getCart() ;
          self.shelf = [];
          self.paymentMethod = ['Cash Account','Cheque'];
          self.payBy;
          self.chequeNumber;
          self.chequeAmount;
          self.user;
          self.contracts;
          self.contractForm={userId:'',
        		  			 paymentMethod:'',
        		  			 booksId:'',
        		  			 chequeNumber: '',
        		  			 chequeAmount: ''}

          self.btnRemoveFromCart = function(index) {
        	  CartService.removeFromCart(index);
        	  self.calTotalPrice();
          }

          self.calTotalPrice = function() {
        	  self.totalPrice = 0.00;
        	  for(var i = 0; i < self.cart.length; i++) {
        		  self.totalPrice += self.cart[i].bookPrice;
        	  }
          }

          self.getUser = function() {
        	  console.log("getUser","start");
        	  UserService.getUser(window.localStorage['userId']).then(
           		   function(d) {
                       self.user = d;
                       console.log("user",self.user);

                  },
                   function(errResponse){
                       console.error('Error while fetching user.');
                   });
          }

          self.validateContract = function() {
        	  let cartItems = self.cart;
        	  let userShelf = self.shelf;

        	  cartItems.map(eachItem => {
        		  userShelf.map(eachBook => {
            		  if (eachItem.bookId == eachBook.bookId){
            			  alert("You have purchase " + eachItem.bookSubject + " (" + eachItem.bookId + ") before. Please check in your book shelf.");
            			  return false;
            		  }
            	  });
        	  });

        	  self.calTotalPrice();
        	  if (self.payBy == 'Cheque') {

        		 if (!self.chequeNumber || self.chequeNumber.length <= 0) {
         			 alert("Please enter a valid cheque number.");
         			 return false;
         		 }

        		 if (isNaN(self.chequeAmount) || Number(self.chequeAmount < 0)) {
        			 alert("Please enter a valid amount.");
        			 return false;
        		 }

         	 } else {
         		 if (self.user.userAcctBalance < self.totalPrice){
         			 alert("Not enough balance");
         			 return false;
         		 }
         	 }

        	 if (self.totalPrice <= 0) {
        		 alert("Less than or equal zero pricing detected.");
        		 return false;
        	 }



        	 return true;
          }

          self.getUserShelf = function(){
        	  var userId = window.localStorage['userId'];
        	  ShelfService.findUserBooks(userId).then(
              		   function(d) {
                           self.shelf = d;
                           console.log("shelf",self.shelf.length);
                      },
                       function(errResponse){
                           console.error('Error while fetching contracts.');
                       });
          }

          self.addContract = function() {
        	 let valid = self.validateContract();
        	 if (!valid) {
        		 console.log('Contract Validation failed');
        		 return false;
        	 }

        	 self.books=[];
        	 for(var i = 0; i < self.cart.length; i++) {
        		 self.books.push(self.cart[i].bookId);
        	 }
        	 self.contractForm.userId = self.user.userId;
        	 self.contractForm.paymentMethod = self.payBy;
        	 self.contractForm.booksId = self.books;
        	 if (self.payBy == 'Cheque') {
        		 self.contractForm.chequeNumber = self.chequeNumber;
        		 self.contractForm.chequeAmount = self.chequeAmount;
        	 }

        	 console.log("contractForm",self.contractForm);
        	 ContractService.addContract(self.contractForm).then(
        			 function(d) {
        				 console.log(d);
        				 $state.go('contract-user');
		            },
		             function(errResponse){
		                 console.error('Error while adding contract.');
		             }
            );

          }

          self.findUserContracts = function() {
        	  var userId = window.localStorage['userId'];
        	  ContractService.findUserContracts(userId).then(
              		   function(d) {
                           self.contracts = d;
                           console.log("contracts",self.contracts.length);
                           console.log("contracts",self.contracts);
                      },
                       function(errResponse){
                           console.error('Error while fetching contracts.');
                       });
          }

          self.void = function(contractNumber) {
        	  console.log(contractNumber)

        	  ContractService.voidContract(contractNumber).then(
        			  function(d) {
        				  alert('Contract Voided')
        				  self.findUserContracts();
        			  },
                      function(errResponse){
                          console.error('Error while voiding contract.');
                      }
        	  )

          }

              self.getUserShelf();
        	  self.calTotalPrice();
              self.getUser();
              self.findUserContracts();

      }]);