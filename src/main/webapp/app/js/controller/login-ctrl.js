'use strict';
angular
.module('jeff.app')
.controller('LoginController', ['$scope', '$state', 'LoginService', function($scope, $state, LoginService) {
          var self = this;
          self.loginAs = ["User","Moderator"];
          self.loginId ='';
          self.user={
        		  userId:null,
        		  userUserName:'',
        		  userPw:'',
        		  userName:'',
        		  userEmail:'',
        		  userTelephone:'',
        		  userMobile:'',
        		  userAddress:'',
        		  userStatus:'',
        		  userCreatedDate:'',
       		  	  userUpdatedDate:'',
       		  	  userApprovedDate:'',
        		  userApprovedBy:'',
        		  userNumberOfRetries:'',
        		  userLastLoginDate:'',
        		  userAcctBalance:''};
          self.moderator={
        		  moderatorId:null,
        		  moderatorUserName:'',
        		  moderatorPw:'',
        		  moderatorEnable:'',
        		  moderatorLastLoginDate:''
          };

          self.login = function() {
        	  self.pw = sha1(self.pw);

        	  if(self.selectedLogin == "User") {
        		  LoginService.getUserId(self.username, self.pw)
      	  			.then(function(result) {
      	  				window.localStorage.clear();
      	  				window.localStorage['userId'] = result.data.userId;
      	  				self.user = result.data;
      	  				console.log('Login as User ', self.user.userId);
      	  				self.loginId = self.user.userId;
      	  			$state.go('book-user');
      	  		},function(errResponse) {
      	  			switch(errResponse.status){
      	  			case 404:
      	  				alert('User not found.');
      	  				break;
      	  			case 403:
      	  				alert('User has been locked or pending approval.');
      	  				break;
      	  			case 401:
      	  				alert('Incorrect password or username.');
      	  				break;

      	  		}
      	  			console.error('Error while login as user');
  	  			})
        	  }

        	  else if(self.selectedLogin == "Moderator") {
        		  LoginService.getModeratorId(self.username, self.pw)
    	  			.then(function(result) {
    	  				window.localStorage.clear();
    	  				window.localStorage['moderatorId'] = result.data.moderatorId;
    	  				self.moderator = result.data;
    	  				console.log('Login as Moderator ', self.moderator.moderatorId);

    	  				self.loginId = self.moderator.moderatorId;
    	  				$state.go('book-moderator');
    	  		},function(errResponse) {console.error('Error while login as moderator');})
        	  };

        	  self.reset();
          };

          self.reset = function(){
        	  self.user={userUserName:'',userPw:''};
        	  self.username = '';
        	  self.pw = '';
              $scope.formLogin.$setPristine(); //reset Form
          };

      }]);