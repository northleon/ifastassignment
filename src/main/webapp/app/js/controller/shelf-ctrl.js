'use strict';

angular
.module('jeff.app')
.controller('ShelfController', ['$scope', 'ShelfService',function($scope,ShelfService) {
          var self = this;

          self.books = [];
          self.accessUserShelf = function() {
        	  let userId = window.localStorage['userId'];
        	  ShelfService.accessUserShelf(userId)
        	  	.then(
        	  			function(d) {
        	  				self.books = d;
        	  				console.log(d)
        	  				console.log('userBooks ' + self.books.length);
        	  			}
        	  	),
                function(errResponse){
                    console.error('Error while fetching books');
                }
          }

          self.accessUserShelf();

      }]);