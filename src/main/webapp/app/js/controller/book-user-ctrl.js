'use strict';

angular
.module('jeff.app')
.controller('BookUserController', ['$scope', 'BookService', 'CartService', function($scope,BookService,CartService) {
          var self = this;
          var isSelected = false;
          var isUser = false;
          var subHeader;
          var isListAll = true;
          var isSearch = false;
          var isEmptyResult = true;
          var searchByChoice;
          
          self.searchBy = ['Subject','Author','Publisher','Category'];
          self.searchBooks=[];
          self.book={bookId:null,
        		  bookSubject:'',
        		  bookDescription:'',
        		  bookIsbn:'',
        		  author:{authorId:'',authorName:'',authorEmail:'',authorCountry:''},
        		  publisher:{publisherId:'',publisherName:'',publisherEmail:'',publisherAddress:'',publisherTelephone:'',publisherFax:''},
        		  category:{categoryId:'',categoryName:'',categoryDescription:''},
        		  bookContent:'',
        		  bookContentType:'',
        		  bookPrice:''};        
          self.books=[];
          
          self.btnAllBook = function(){
          	 	self.subHeader = 'All Book';
          	 	self.isListAll = true;
          	 	self.isSearch = false;
          	 	self.isSelected = false;
          	 	self.searchValue = '';
          	 	self.searchBooks = null;
          };
          
          self.btnSearchBook = function(){
        	 	self.subHeader = 'Search Book';
        	 	self.isListAll = false;
        	 	self.isSearch = true;
        	 	self.isSelected = false;
          };
          
          self.btnAddToCart = function(u){
        	  CartService.addToCart(u);
          };
          
          self.btnViewBook = function(u){
        	  self.book = u; 
        	  self.isSelected = !self.isSelected;
          };
          
          self.btnViewBookBack = function(u){
        	  self.book = null; 
        	  self.isSelected = !self.isSelected;
          };
          
          self.showSearchResult = function(){
        	  if(!self.isEmptyResult)
        		  if(self.isSearch)
        			  return true;
        	  return false;
          };
          
          self.searchBook = function(criteria, value){
         	 BookService.searchBook(criteria, value)
                       .then(
                    		   function(d) {
                                   self.searchBooks = d;
                                   console.log('searchBooks '+self.searchBooks.length);
                                   console.log("searchBooks",self.searchBooks);
                                   if(self.searchBooks.length == 0) {
                                	   self.isEmptyResult = true;
                                   }
                                   else if(self.searchBooks.length > 0) {
                                	   self.isEmptyResult = false;
                                   }
                                	   
                                   if(!self.isEmptyResult)
                                	   self.isListAll = true;
                              },
                               function(errResponse){
                                   console.error('Error while fetching books');
                               }
                   );
           };

          self.findAllBook = function(){
        	  self.subHeader = 'All Book';
        	  self.isListAll = true;
              BookService.findAllBook()
                  .then(
                               function(d) {
                                    self.books = d;
                                    console.log('books '+self.books.length);
                                    console.log(self.books);
                               },
                                function(errResponse){
                                    console.error('Error while fetching books');
                                }
                       );
          };
          
          if(window.localStorage['userId'] != "null") {
	      		self.isUser = true;
      	  }
          else {
        	  self.isUser = false;
          }
          self.findAllBook();                    
      }]);