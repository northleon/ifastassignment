package org.jeff.service;

import java.util.List;

import javax.jws.WebService;

import org.jeff.domain.entity.Publisher;

@WebService
public interface PublisherService {
	public void addPublisher(Publisher publisher);
	public void updatePublisher(Publisher publisher);
	
	public String getPublisherName(final String publisherId);
	public String getPublisherEmail(final String publisherId);
	public String getPublisherAddress(final String publisherId);
	public String getPublisherTelephone(final String publisherId);
	public String getPublisherFax(final String publisherId);
	
	public List<Publisher> findAllPublisher();
	public Publisher getPublisherById(final String id);
}
