package org.jeff.service;

import java.util.Date;
import java.util.List;

import javax.jws.WebService;

import org.jeff.domain.entity.Contract;
import org.jeff.domain.entity.Moderator;

@WebService
public interface ContractService {
	public void addContract(Contract contract);
	public void updateContractUpdatedDate(Contract contract);
	public void updateContractVoidedDate(Contract contract);
	public void updateContractStatus(Contract contract);
	public void approveContract(Contract contract);
	public void rejectContract(Contract contract);
	public void voidContract(Contract contract);

	public List<Contract> findUserContracts(final String userId);
	public Date getContractCreatedDate(final String contractNumber);
	public Date getContractUpdatedDate(final String contractNumber);
	public Date getContractVoidedDate(final String contractNumber);
	public int getContractStatus(final String contractNumber);
	public Moderator getContractApprovedBy(final String contractNumber);
	public String getContractPaymentMethod(final String contractNumber);

	public List<Contract> findAllContract();
	public Contract getContractById(final String id);
}
