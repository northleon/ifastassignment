package org.jeff.service;

import java.util.List;

import javax.jws.WebService;

import org.jeff.domain.entity.Category;

@WebService
public interface CategoryService {
	public void addCategory(Category category);
	public void updateCategory(Category category);
	
	public String getCategoryName(final String categoryId);
	public String getDescription(final String categoryId);
	
	public List<Category> findAllCategory();
	public Category getCategoryById(final String id);
}
