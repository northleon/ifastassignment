package org.jeff.service;

import java.util.List;

import javax.jws.WebService;

import org.jeff.domain.entity.User;

@WebService
public interface UserService {
	void addUser(final User user);
	void updateUser(final User user);
	void deleteByUserId(final String userId);
	void approveUser(final User user);
	void unlockUser(final User user);
	void lockUser(final User user);
	void updateUserLoginDate(final User user);
	void updateUserNumberOfRetries(final User user);

	public String getUserId(final String userUserName, final String userPw);
	public double getUserAcctBalance(final String userId);
	User getById(final String userId);
	User getByUsername(final String username);
	public boolean isUserExist(final String username);
	List<User> findAllUser();
}
