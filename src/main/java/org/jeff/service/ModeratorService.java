package org.jeff.service;

import java.util.List;

import javax.jws.WebService;

import org.jeff.domain.entity.Moderator;

@WebService
public interface ModeratorService {
	void addModerator(final Moderator moderator);
	void updateModerator(final Moderator moderator);
	void deleteByModeratorId(final String moderatorId);
	void updateModeratorLoginDate(final Moderator moderator);
	
	public String getModeratorId(final String moderatorUsername, final String moderatorPw);
	Moderator getById(final String moderatorId);
	List<Moderator> findAllModerator();
}
