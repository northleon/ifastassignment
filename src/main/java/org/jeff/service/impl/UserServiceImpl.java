package org.jeff.service.impl;

import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.jeff.constant.UserStatus;
import org.jeff.domain.dao.UserDao;
import org.jeff.domain.entity.User;
import org.jeff.service.UserService;
import org.jeff.util.Util;

@Service
public class UserServiceImpl implements UserService{

	@Inject
	private UserDao userDao;

	@Override
	@Transactional
	public void addUser(User user) {
		int id = findAllUser().size() + 1;
		user.setUserId(Util.generateId("User",id));
		userDao.add(user);
	}

	@Override
	@Transactional
	public void updateUser(User user) {
		userDao.update(user);
	}

	@Override
	@Transactional
	public void deleteByUserId(String userId) {
		userDao.deleteById(userId);
	}

	@Override
	@Transactional
	public void approveUser(User user) {
		userDao.update(user);
	}

	@Override
	@Transactional
	public void unlockUser(User user) {
		userDao.update(user);
	}

	@Override
	@Transactional
	public void lockUser(User user) {
		user.setUserStatus(UserStatus.Locked.value());
		userDao.update(user);
	}

	@Override
	@Transactional
	public void updateUserLoginDate(User user) {
		user.setUserNumberOfRetries(0);
		user.setUserLastLoginDate(new Date());
		userDao.update(user);
	}

	@Override
	@Transactional
	public void updateUserNumberOfRetries(User user) {
		int numOfRetries = user.getUserNumberOfRetries() + 1;
		if (numOfRetries > 5)
			user.setUserStatus(UserStatus.Locked.value());

		user.setUserNumberOfRetries(numOfRetries);
		userDao.update(user);
	}

	@Override
	@Transactional(readOnly = true)
	public String getUserId(String userUserName, String userPw) {
		return userDao.getUserId(userUserName, userPw);
	}

	@Override
	@Transactional(readOnly = true)
	public double getUserAcctBalance(String userId) {
		return userDao.getUserAcctBalance(userId);
	}

	@Override
	@Transactional(readOnly = true)
	public User getById(String userId) {
		return userDao.getById(userId);
	}

	@Override
	@Transactional(readOnly = true)
	public List<User> findAllUser() {
		return userDao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public User getByUsername(String username) {
		return userDao.getByUsername(username);
	}

	@Override
	@Transactional(readOnly = true)
	public boolean isUserExist(String username) {
		User obj = userDao.getByUsername(username);
		return obj != null;
	}
}
