package org.jeff.service.impl;

import java.util.List;

import javax.inject.Inject;

import org.jeff.domain.dao.ModeratorDao;
import org.jeff.domain.entity.Moderator;
import org.jeff.service.ModeratorService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ModeratorServiceImpl implements ModeratorService{

	@Inject
	private ModeratorDao moderatorDao;
	
	@Override
	@Transactional
	public void addModerator(Moderator moderator) {
		moderatorDao.add(moderator);
	}

	@Override
	@Transactional
	public void updateModerator(Moderator moderator) {
		moderatorDao.update(moderator);
	}

	@Override
	@Transactional
	public void deleteByModeratorId(String moderatorId) {
		moderatorDao.deleteById(moderatorId);
	}

	@Override
	@Transactional
	public void updateModeratorLoginDate(Moderator moderator) {
		moderatorDao.update(moderator);
	}

	@Override
	@Transactional(readOnly=true)
	public String getModeratorId(String moderatorUsername, String moderatorPw) {
		return moderatorDao.getModeratorId(moderatorUsername, moderatorPw);
	}

	@Override
	@Transactional(readOnly=true)
	public Moderator getById(String moderatorId) {
		return moderatorDao.getById(moderatorId);
	}

	@Override
	@Transactional(readOnly=true)
	public List<Moderator> findAllModerator() {
		return moderatorDao.findAll();
	}

}
