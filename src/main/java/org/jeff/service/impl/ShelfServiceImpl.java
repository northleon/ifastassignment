package org.jeff.service.impl;

import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import org.jeff.domain.dao.ShelfDao;
import org.jeff.domain.entity.Book;
import org.jeff.domain.entity.Shelf;
import org.jeff.service.ShelfService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ShelfServiceImpl implements ShelfService{

	@Inject
	private ShelfDao shelfDao;

	@Override
	@Transactional
	public void addBooks(List<Book> books, Shelf shelf) {
		/*shelf.setBooks(books);
		shelfDao.add(shelf);*/
	}

	@Override
	@Transactional
	public void updateShelfNumberOfBook(Shelf shelf) {
		shelf.setShelfNumberOfBook(shelf.getBooks().size());
		shelfDao.update(shelf);
	}

	@Override
	@Transactional
	public void updateShelfLastAccessDate(Shelf shelf) {
		shelf.setShelfLastAccessDate(new Date());
		shelfDao.update(shelf);
	}

	@Override
	@Transactional
	public void updateNumberOfAccess(Shelf shelf) {
		shelf.setShelfNumberOfAccess(shelf.getShelfNumberOfAccess() + 1);;
		shelfDao.update(shelf);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Book> getBooks(String shelfUser) {
		return shelfDao.getBooks(shelfUser);
	}

	@Override
	@Transactional(readOnly = true)
	public int getShelfNumberOfBooks(String shelfUser) {
		return shelfDao.getShelfNumberOfBooks(shelfUser);
	}

	@Override
	@Transactional(readOnly = true)
	public Date getShelfLastAccesDate(String shelfUser) {
		return shelfDao.getShelfLastAccesDate(shelfUser);
	}

	@Override
	@Transactional(readOnly = true)
	public int getShelfNumberOfAccess(String shelfUser) {
		return shelfDao.getShelfNumberOfAccess(shelfUser);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Shelf> getAllShelf() {
		return shelfDao.findAll();
	}

	@Override
	@Transactional
	public Shelf getShelfById(Long id) {
		return shelfDao.getById(id);
	}

	@Override
	@Transactional(readOnly = true)
	public Long getShelfIdByUserId(String shelfUser) {
		return shelfDao.getShelfIdByUserId(shelfUser);
	}
}
