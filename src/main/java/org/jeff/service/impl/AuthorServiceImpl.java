package org.jeff.service.impl;

import java.util.List;
import org.jeff.util.Util;

import javax.inject.Inject;

import org.jeff.domain.dao.AuthorDao;
import org.jeff.domain.entity.Author;
import org.jeff.service.AuthorService;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Service("authorService")
public class AuthorServiceImpl implements AuthorService{

	@Inject
	private AuthorDao authorDao;

	@Override
	@Transactional
	public void addAuthor(Author author) {
		int id = findAllAuthor().size() + 1;
		author.setAuthorId(Util.generateId("Author",id));
		authorDao.add(author);
	}

	@Override
	@Transactional
	public void updateAuthor(Author author) {
		authorDao.update(author);
	}

	@Override
	@Transactional(readOnly = true)
	public String getAuthorName(String authorId) {
		return authorDao.getAuthorName(authorId);
	}

	@Override
	@Transactional(readOnly = true)
	public String getAuthorEmail(String authorId) {
		return authorDao.getAuthorEmail(authorId);
	}

	@Override
	@Transactional(readOnly = true)
	public String getAuthorCountry(String authorId) {
		return authorDao.getAuthorCountry(authorId);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Author> findAllAuthor() {
		return authorDao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Author getAuthorById(String id) {
		return authorDao.getById(id);
	}
}
