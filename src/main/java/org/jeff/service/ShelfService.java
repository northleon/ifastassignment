package org.jeff.service;

import java.util.Date;
import java.util.List;

import javax.jws.WebService;

import org.jeff.domain.entity.Book;
import org.jeff.domain.entity.Shelf;

@WebService
public interface ShelfService {
	void addBooks(final List<Book> books, final Shelf shelf);
	void updateShelfNumberOfBook(final Shelf shelf);
	void updateShelfLastAccessDate(final Shelf shelf);
	void updateNumberOfAccess(final Shelf shelf);

	public List<Book> getBooks(final String shelfUser);
	public int getShelfNumberOfBooks(final String shelfUser);
	public Date getShelfLastAccesDate(final String shelfUser);
	public int getShelfNumberOfAccess(final String shelfUser);

	public List<Shelf> getAllShelf();
	public Shelf getShelfById(final Long id);
	public Long getShelfIdByUserId(final String shelfUser);
}
