package org.jeff.constant;

import java.util.Optional;
import java.util.stream.Stream;

public enum ChequeStatus implements EnumConstant{
	Confirmed("confirmed","Cheque Confirmed"),
	Pending("pending","Cheque Pending"),
	Reject("rejected","Cheque Rejected"),
	;
	String code;
	String desc;

	ChequeStatus(final String code, final String desc){
		this.code = code;
		this.desc = desc;
	}

	public static Optional<ChequeStatus> find(String code) {
		return Stream.of(ChequeStatus.values()).filter(each -> each.code.equalsIgnoreCase(code)).findFirst();
	}

	@Override
	public String value() {
		return this.code;
	}

	@Override
	public String desc() {
		return this.desc;
	}
}
