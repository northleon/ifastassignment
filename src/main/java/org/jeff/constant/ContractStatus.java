package org.jeff.constant;

import java.util.Optional;
import java.util.stream.Stream;

public enum ContractStatus implements EnumConstant{
	Confirmed("confirmed","Contract Confirmed"),
	Pending("pending","Contract Pending"),
	Voided("voided","Contract Voided"),
	;
	String code;
	String desc;

	ContractStatus(final String code, final String desc){
		this.code = code;
		this.desc = desc;
	}

	public static Optional<ContractStatus> find(String code) {
		return Stream.of(ContractStatus.values()).filter(each -> each.code.equalsIgnoreCase(code)).findFirst();
	}

	@Override
	public String value() {
		return this.code;
	}

	@Override
	public String desc() {
		return this.desc;
	}
}
