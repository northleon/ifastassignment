package org.jeff.constant;

public interface EnumConstant {
	public String value();
	public String desc();

}
