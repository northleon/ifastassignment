package org.jeff.constant;

import java.util.Optional;
import java.util.stream.Stream;

public enum UserStatus implements EnumConstant{
	Confirmed("confirmed","Account Confirmed"),
	Pending("pending","Pending Approval"),
	Locked("locked","Account Locked"),
	;
	String code;
	String desc;

	UserStatus(final String code, final String desc){
		this.code = code;
		this.desc = desc;
	}

	public static Optional<UserStatus> find(String code) {
		return Stream.of(UserStatus.values()).filter(each -> each.code.equalsIgnoreCase(code)).findFirst();
	}

	@Override
	public String value() {
		return this.code;
	}

	@Override
	public String desc() {
		return this.desc;
	}
}
