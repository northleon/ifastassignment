package org.jeff.constant;

import java.util.Optional;
import java.util.stream.Stream;

public enum PaymentMethod implements EnumConstant{
	Cheque("Cheque","Cheque Payment Method"),
	Cash_Account("Cash Account","Cash Payment Method"),
	;

	String code;
	String desc;

	PaymentMethod(final String code, final String desc){
		this.code = code;
		this.desc = desc;
	}

	public static Optional<PaymentMethod> find(String code) {
		return Stream.of(PaymentMethod.values()).filter(each -> each.code.equalsIgnoreCase(code)).findFirst();
	}

	@Override
	public String value() {
		return this.code;
	}

	@Override
	public String desc() {
		return this.desc;
	}

}
