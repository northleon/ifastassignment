package org.jeff.domain.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "LEON_SHELF")
public class Shelf implements Serializable {
	/**
	 *
	 */
	private static final long serialVersionUID = -6766692678446152759L;
	@Id
	@GeneratedValue(generator = "sequence", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "sequence", sequenceName = "LEON_SHELF_SEQUENCE", allocationSize = 1)
	@Column(name = "SHELF_ID")
	private Long shelfId;
	@OneToOne(fetch = FetchType.LAZY)
//	@PrimaryKeyJoinColumn
	@JoinColumn(name = "SHELF_USER")
	private User shelfUser;
	@Column(name = "SHELF_NUMBER_OF_BOOK")
	private int shelfNumberOfBook;
	@Column(name = "SHELF_LAST_ACCESS_DATE")
	private Date shelfLastAccessDate;
	@Column(name = "SHELF_NUMBER_OF_ACCESS")
	private int shelfNumberOfAccess;

	//Composite Table
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "LEON_SHELF_BOOK", joinColumns = { @JoinColumn(name = "SHELF_BOOK_SHELF_ID") }, inverseJoinColumns = {
			@JoinColumn(name = "SHELF_BOOK_BOOK_ID") })
	private Set<Book> books = new HashSet<Book>();

	public Set<Book> getBooks() {
		return books;
	}

	public void setBooks(List<Book> listBooks) {
		Set<Book> books= new HashSet<Book>(listBooks);
		this.books = books;
	}

	public User getShelfUser() {
		return shelfUser;
	}

	public void setShelfUser(User shelfUser) {
		this.shelfUser = shelfUser;
	}

	public int getShelfNumberOfBook() {
		return shelfNumberOfBook;
	}

	public void setShelfNumberOfBook(int shelfNumberOfBook) {
		this.shelfNumberOfBook = shelfNumberOfBook;
	}

	public Date getShelfLastAccessDate() {
		return shelfLastAccessDate;
	}

	public void setShelfLastAccessDate(Date shelfLastAccessDate) {
		this.shelfLastAccessDate = shelfLastAccessDate;
	}

	public int getShelfNumberOfAccess() {
		return shelfNumberOfAccess;
	}

	public void setShelfNumberOfAccess(int shelfNumberOfAccess) {
		this.shelfNumberOfAccess = shelfNumberOfAccess;
	}

	public Long getShelfId() {
		return shelfId;
	}

	public void setShelfId(Long shelfId) {
		this.shelfId = shelfId;
	}

}
