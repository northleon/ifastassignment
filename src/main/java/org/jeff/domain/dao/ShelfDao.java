package org.jeff.domain.dao;

import java.util.Date;
import java.util.List;
import org.jeff.domain.entity.Book;
import org.jeff.domain.entity.Shelf;
import org.springframework.stereotype.Repository;

@Repository
public interface ShelfDao extends GenericDao<Shelf, Long>{
	public List<Book> getBooks(final String shelfUser);
	public int getShelfNumberOfBooks(final String shelfUser);
	public Date getShelfLastAccesDate(final String shelfUser);
	public int getShelfNumberOfAccess(final String shelfUser);
	public Long getShelfIdByUserId(final String shelfUser);
}
