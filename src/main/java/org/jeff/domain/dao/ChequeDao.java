package org.jeff.domain.dao;

import java.util.List;

import org.jeff.domain.entity.Cheque;
import org.springframework.stereotype.Repository;

@Repository
public interface ChequeDao extends GenericDao<Cheque, String>{
	public List<Cheque> getCheques(final String userId);
	public double getChequeAmt(final String contractNumber);
	public int getChequeStatus(final String contractNumber);
	public String getChequeNumber(final String contractNumber);
}
