package org.jeff.domain.dao.impl;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.jeff.domain.dao.ContractDao;
import org.jeff.domain.entity.Contract;
import org.jeff.domain.entity.Moderator;
import org.jeff.domain.entity.User;
import org.springframework.stereotype.Repository;

@Repository
public class ContractDaoImpl extends AbstractGenericDao<Contract, String> implements ContractDao{
	private final static String TABLE = "LEON_CONTRACT";

	@SuppressWarnings("unchecked")
	@Override
	public List<Contract> findUserContracts(String userId) {
		Criteria cr = getSession().createCriteria(Contract.class)
				.add(Restrictions.eq("contractUser.userId", userId))
				.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		Object contracts = cr.list();
		List<Contract> result = (List<Contract>) contracts;
		return result;
	}

	@Override
	public int getContractStatus(String contractNumber) {
		Query query = getSession().createQuery("SELECT CONTRACT_STATUS FROM "+ TABLE +" WHERE CONTRACT_NUMBER = "+ contractNumber);
		int result = (int) query.uniqueResult();
		return result;
	}

	@Override
	public String getContractPaymentMethod(String contractNumber) {
		Query query = getSession().createQuery("SELECT CONTRACT_PAYMENT_METHOD FROM "+ TABLE +" WHERE CONTRACT_NUMBER = "+ contractNumber);
		String result = (String) query.uniqueResult();
		return result;
	}

	@Override
	public Date getContractCreatedDate(String contractNumber) {
		Query query = getSession().createQuery("SELECT CONTRACT_CREATED_DATE FROM "+ TABLE +" WHERE CONTRACT_NUMBER = "+ contractNumber);
		Date result = (Date) query.uniqueResult();
		return result;
	}

	@Override
	public Date getContractUpdatedDate(String contractNumber) {
		Query query = getSession().createQuery("SELECT CONTRACT_UPDATED_DATE FROM "+ TABLE +" WHERE CONTRACT_NUMBER = "+ contractNumber);
		Date result = (Date) query.uniqueResult();
		return result;
	}

	@Override
	public Date getContractVoidedDate(String contractNumber) {
		Query query = getSession().createQuery("SELECT CONTRACT_VOIDED_DATE FROM "+ TABLE +" WHERE CONTRACT_NUMBER = "+ contractNumber);
		Date result = (Date) query.uniqueResult();
		return result;
	}

	@Override
	public Moderator getContractApprovedBy(String contractNumber) {
		Query query = getSession().createQuery("SELECT CONTRACT_APPROVED_BY FROM "+ TABLE +" WHERE CONTRACT_NUMBER = "+ contractNumber);
		Moderator result = (Moderator) query.uniqueResult();
		return result;
	}

	@Override
	public List<Contract> findAllContract() {
		Criteria cr = getSession().createCriteria(Contract.class)
				.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		Object contracts = cr.list();
		List<Contract> result = (List<Contract>) contracts;
		return result;
	}

}
