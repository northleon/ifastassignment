package org.jeff.domain.dao.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.transform.Transformers;
import org.jeff.domain.dao.ShelfDao;
import org.jeff.domain.entity.Book;
import org.jeff.domain.entity.Shelf;
import org.springframework.stereotype.Repository;

@Repository
public class ShelfDaoImpl extends AbstractGenericDao<Shelf, Long> implements ShelfDao{
	private final static String TABLE = "LEON_SHELF";

	@SuppressWarnings("unchecked")
	@Override
	public List<Book> getBooks(String shelfUser) {
		String subTable = "LEON_BOOK";
		String subQuery = "SELECT SHELF_ID FROM " + TABLE + " WHERE SHELF_USER = '" + shelfUser + "'";
		String subSubQuery = "SELECT SHELF_BOOK_BOOK_ID FROM LEON_SHELF_BOOK WHERE SHELF_BOOK_SHELF_ID = (" + subQuery + ")";
		Query query = getSession().createSQLQuery("SELECT A.* FROM "+ subTable +" A WHERE BOOK_ID IN ("+subSubQuery+")")
					.addEntity(Book.class);


		List<Book> result = query.list();
		return result;
	}

	@Override
	public int getShelfNumberOfBooks(String shelfUser) {
		Query query = getSession().createQuery("SELECT SHELF_NUMBER_OF_BOOK FROM "+ TABLE +" WHERE SHELF_USER = "+ shelfUser);
		int result = (int) query.uniqueResult();
		return result;
	}

	@Override
	public Date getShelfLastAccesDate(String shelfUser) {
		Query query = getSession().createQuery("SELECT SHELF_LAST_ACCESS_DATE FROM "+ TABLE +" WHERE SHELF_USER = "+ shelfUser);
		Date result = (Date) query.uniqueResult();
		return result;
	}

	@Override
	public int getShelfNumberOfAccess(String shelfUser) {
		Query query = getSession().createQuery("SELECT SHELF_NUMBER_OF_ACCESS FROM "+ TABLE +" WHERE SHELF_USER = "+ shelfUser);
		int result = (int) query.uniqueResult();
		return result;
	}

	@Override
	public Long getShelfIdByUserId(String shelfUser) {
		Query query = getSession().createSQLQuery("SELECT SHELF_ID FROM "+ TABLE +" WHERE SHELF_USER = '"+ shelfUser + "'");
		BigDecimal result = (BigDecimal) query.uniqueResult();
		return result.longValue();
	}
}
