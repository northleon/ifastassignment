package org.jeff.web.role;

import java.util.Optional;
import java.util.stream.Stream;

public enum Role {
	Guest,
	User,
	Moderator,
	;

	public static Optional<Role> find(String role) {
		return Stream.of(Role.values()).filter(each -> each.toString().equalsIgnoreCase(role)).findFirst();
	}

}
