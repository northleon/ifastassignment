package org.jeff.web.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.inject.Inject;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.jeff.domain.entity.Book;
import org.jeff.domain.entity.Shelf;
import org.jeff.service.ShelfService;

@RequestMapping(value = "/")
@RestController
public class ShelfController {

	@Inject
    ShelfService shelfservice;

	 //-------------------Retrieve User books--------------------------------------------------------
    @RequestMapping(value = "/shelf-books/{userId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Book>> getBook(@PathVariable("userId") String userId) {
        System.out.println("Fetching book with userId " + userId);

        long shelfId = shelfservice.getShelfIdByUserId(userId);
        Shelf userShelf = shelfservice.getShelfById(shelfId);

        List<Book> books = userShelf.getBooks().stream().collect(Collectors.toList());
        if (books.isEmpty()) {
            System.out.println("Books with userId " + userId + " not found");
            return new ResponseEntity<List<Book>>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<List<Book>>(books, HttpStatus.OK);
    }

  //-------------------Retrieve User books--------------------------------------------------------
    @RequestMapping(value = "/shelf-user/{userId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Book>> accessUserBookShelf(@PathVariable("userId") String userId) {
        System.out.println("Fetching book with userId " + userId);

        // update user shelf access
        long shelfId = shelfservice.getShelfIdByUserId(userId);
        Shelf userShelf = shelfservice.getShelfById(shelfId);

        shelfservice.updateNumberOfAccess(userShelf);
        shelfservice.updateShelfLastAccessDate(userShelf);

        List<Book> books = userShelf.getBooks().stream().collect(Collectors.toList());

        if (books.isEmpty()) {
            System.out.println("Books with userId " + userId + " not found");
            return new ResponseEntity<List<Book>>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<List<Book>>(books, HttpStatus.OK);
    }
}