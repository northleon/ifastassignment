package org.jeff.web.controller;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.inject.Inject;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;
import org.jeff.constant.UserStatus;
import org.jeff.domain.entity.Moderator;
import org.jeff.domain.entity.Shelf;
import org.jeff.domain.entity.User;
import org.jeff.service.ModeratorService;
import org.jeff.service.UserService;
import org.jeff.util.Util;
import org.jeff.web.role.Role;
import org.jeff.web.webform.UserStatusForm;
@RequestMapping(value = "/")
@RestController
public class UserController {

    @Inject
    UserService userService;  //Service which will do all data retrieval/manipulation work

    @Inject
    ModeratorService moderatorService;


    //-------------------Retrieve All Users---------------book-ctrl.js-----------------------------------------

    @RequestMapping(value = "/user/", method = RequestMethod.GET)
    public ResponseEntity<List<User>> listAllUser() {
        List<User> users = userService.findAllUser();
        if(users.isEmpty()){
            return new ResponseEntity<List<User>>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        return new ResponseEntity<List<User>>(users, HttpStatus.OK);
    }



    //-------------------Retrieve Single user--------------------------------------------------------

    @RequestMapping(value = "/user/{userId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<User> getUser(@PathVariable("userId") String userId) {
        System.out.println("Fetching user with userId " + userId);
        User user = userService.getById(userId);
        if (user == null) {
            System.out.println("user with userId " + userId + " not found");
            return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<User>(user, HttpStatus.OK);
    }

/*  //-------------------Retrieve Single user acct balance--------------------------------------------------------

    @RequestMapping(value = "/user/{userId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<double> getUserAcctBalance(@PathVariable("id") String userId) {
        System.out.println("Fetching user with userId " + userId);
        double acctBalance = userService.getUserAcctBalance(userId);
        if (acctBalance == -1) {
            System.out.println("user with userId " + userId + " not found");
            return new ResponseEntity<double>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<double>(acctBalance, HttpStatus.OK);
    }*/

    //-------------------User login--------------------------------------------------------
    @RequestMapping(value = "/user-login/", method = RequestMethod.GET)
    public ResponseEntity<User> getUserId(@RequestParam(value = "username") String userUsername, @RequestParam(value = "pw") String userPw) {
        System.out.println("Login user with username -"+userUsername+",userPw -"+userPw);

        //Get By userName
        User user = userService.getByUsername(userUsername);
        if(user == null) {
        	System.out.println("Login status : N");
            return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
        } else {
        	String dbPw = user.getUserPw();
        	if (!dbPw.equalsIgnoreCase(userPw)) { // wrong password
        		//update retries count
        		userService.updateUserNumberOfRetries(user);

        		System.out.println("Login status : N");
		    	return new ResponseEntity<User>(HttpStatus.UNAUTHORIZED);
        	} else {
        		//Check user status to verify login
            	Optional<UserStatus> userStatus = UserStatus.find(user.getUserStatus());
            	if(userStatus.isPresent()){
        			switch (userStatus.get()) {
        			case Confirmed: {
        				System.out.println("Login status : Y");
        				userService.updateUserLoginDate(user);
        		    	return new ResponseEntity<User>(user, HttpStatus.OK);
        			}
        			case Pending:
        			case Locked:
        			default: {
        				System.out.println("Login status : N");
        		    	return new ResponseEntity<User>(HttpStatus.FORBIDDEN);
        			}

        			}
            	} else {
            		System.out.println("Login status : N");
                    return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
            	}
        	}
        }

    }



    //-------------------Create a user--------------------------------------------------------

    @RequestMapping(value = "/user/", method = RequestMethod.POST)
    public ResponseEntity<Void> addUser(@RequestBody User user,    UriComponentsBuilder ucBuilder) {
        System.out.println("Creating user " + user.getUserName());

        if (userService.isUserExist(user.getUserUsername())) {
            System.out.println("A user with name " + user.getUserUsername() + " already exist");
            return new ResponseEntity<Void>(HttpStatus.CONFLICT);
        }
		Date today = new Date();
		user.setUserCreatedDate(today);
		user.setUserStatus(UserStatus.Pending.value());
		user.setUserPw(user.getUserPw());

		// create User shelf
		Shelf userShelf = new Shelf();
		userShelf.setShelfUser(user);
		user.setShelf(userShelf);

        userService.addUser(user);

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/user/{userId}").buildAndExpand(user.getUserId()).toUri());
        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
    }



    //------------------- Update a user --------------------------------------------------------

    @RequestMapping(value = "/user/{userId}", method = RequestMethod.PUT)
    public ResponseEntity<User> updateUser(@PathVariable("userId") String userId, @RequestBody User user) {
        System.out.println("Updating author " + userId);

        User currentUser = userService.getById(userId);

        if (currentUser==null) {
            System.out.println("user with id " + userId + " not found");
            return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
        }

        currentUser.setUserPw(user.getUserPw());
        currentUser.setUserName(user.getUserName());
        currentUser.setUserEmail(user.getUserEmail());
        currentUser.setUserTelephone(user.getUserTelephone());
        currentUser.setUserMobile(user.getUserMobile());
        currentUser.setUserAddress(user.getUserAddress());

        userService.updateUser(currentUser);
        return new ResponseEntity<User>(currentUser, HttpStatus.OK);
    }

    @RequestMapping(value = "/user/update-status", method = RequestMethod.PUT)
    public ResponseEntity<Void> updateUserStatus(@RequestBody UserStatusForm userstatusForm, @RequestHeader(value="User-Role", defaultValue="guest") String role) {
    	System.out.println(role);
        System.out.println("Updating User Status " + userstatusForm);

        Optional<Role> accessRole = Role.find(role);

        if (accessRole.isPresent()) {
        	User currentUser = userService.getById(Util.notNull.test(userstatusForm.getUserId()) ? userstatusForm.getUserId() : "");
            Moderator moderator = moderatorService.getById(Util.notNull.test(userstatusForm.getModeratorId()) ? userstatusForm.getModeratorId() : "");

            if (currentUser==null) {
                System.out.println("user with id " + userstatusForm.getUserId() + " not found");
                return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
            }

            if (moderator == null) {
            	System.out.println("moderator with id " + userstatusForm.getModeratorId() + " not found.");
            	return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
            }

            Optional<UserStatus> status = UserStatus.find(userstatusForm.getChangeStatus());
            if (status.isPresent()){
            	//update user status
            	currentUser.setUserStatus(status.get().value());
            	currentUser.setUserApprovedBy(moderator);
            	currentUser.setUserApprovedDate(new Date());
            	currentUser.setUserUpdatedDate(new Date());

                userService.updateUser(currentUser);
                return new ResponseEntity<Void>(HttpStatus.OK);
            } else {
                return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);
            }
        } else {
        	return new ResponseEntity<Void>(HttpStatus.UNAUTHORIZED);
        }


    }

/*    //------------------- Delete a author --------------------------------------------------------

    @RequestMapping(value = "/author/{authorId}", method = RequestMethod.DELETE)
    public ResponseEntity<Author> deleteAuthor(@PathVariable("id") long id) {
        System.out.println("Fetching & Deleting User with id " + id);

        User user = authorService.findById(id);
        if (user == null) {
            System.out.println("Unable to delete. User with id " + id + " not found");
            return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
        }

        authorService.deleteUserById(id);
        return new ResponseEntity<User>(HttpStatus.NO_CONTENT);
    }



    //------------------- Delete All author --------------------------------------------------------

    @RequestMapping(value = "/user/", method = RequestMethod.DELETE)
    public ResponseEntity<User> deleteAllUsers() {
        System.out.println("Deleting All Users");

        authorService.deleteAllUsers();
        return new ResponseEntity<User>(HttpStatus.NO_CONTENT);
    }*/

}