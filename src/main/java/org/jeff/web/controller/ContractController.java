package org.jeff.web.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.inject.Inject;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;
import org.jeff.constant.ChequeStatus;
import org.jeff.constant.ContractStatus;
import org.jeff.constant.PaymentMethod;
import org.jeff.domain.entity.Book;
import org.jeff.domain.entity.CashAcctTrans;
import org.jeff.domain.entity.Cheque;
import org.jeff.domain.entity.Contract;
import org.jeff.domain.entity.Moderator;
import org.jeff.domain.entity.User;
import org.jeff.service.BookService;
import org.jeff.service.CashAcctTransService;
import org.jeff.service.ChequeService;
import org.jeff.service.ContractService;
import org.jeff.service.ModeratorService;
import org.jeff.service.ShelfService;
import org.jeff.service.UserService;
import org.jeff.web.webform.ContractForm;

@RequestMapping(value = "/")
@RestController
public class ContractController {

    @Inject
    BookService bookService;  //Service which will do all data retrieval/manipulation work
    @Inject
    UserService userService;
    @Inject
    ContractService contractService;
    @Inject
    ModeratorService moderatorService;
    @Inject
    ShelfService shelfService;
    @Inject
    ChequeService chequeService;
    @Inject
    CashAcctTransService cashAccTransService;

    //-------------------Retrieve All Contracts--------------------------------------------------------

    @RequestMapping(value = "/contract-moderator/", method = RequestMethod.GET)
    public ResponseEntity<List<Contract>> listAllContract() {
        List<Contract> contracts = contractService.findAllContract();
        if(contracts.isEmpty()){
            return new ResponseEntity<List<Contract>>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        return new ResponseEntity<List<Contract>>(contracts, HttpStatus.OK);
    }

  //-------------------Retrieve User Contracts--------------------------------------------------------

    @RequestMapping(value = "/contract-user/{userId}", method = RequestMethod.GET)
    public ResponseEntity<List<Contract>> listUserContract(@PathVariable("userId") String userId) {
        List<Contract> contracts = contractService.findUserContracts(userId);
        System.out.println("listUserContract size - "+contracts.size());
        if(contracts.isEmpty()){
            return new ResponseEntity<List<Contract>>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        return new ResponseEntity<List<Contract>>(contracts, HttpStatus.OK);
    }

/*    //-------------------Retrieve Single book--------------------------------------------------------

    @RequestMapping(value = "/book-list/{bookId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Book> getBook(@PathVariable("id") String bookId) {
        System.out.println("Fetching book with bookId " + bookId);
        Book book = bookService.getBookById(bookId);
        if (book == null) {
            System.out.println("book with bookId " + bookId + " not found");
            return new ResponseEntity<Book>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Book>(book, HttpStatus.OK);
    }*/



    //-------------------Create a contract--------------------------------------------------------

    @RequestMapping(value = "/contract-add/", method = RequestMethod.POST)
    public ResponseEntity<Void> addContract(@RequestBody ContractForm contractForm, UriComponentsBuilder ucBuilder) {
    	String userId = contractForm.getUserId();
    	String paymentMethod = contractForm.getPaymentMethod();
    	String[] booksId = contractForm.getBooksId();

    	User user = userService.getById(userId);
    	Contract contract = new Contract();
    	List<Book> book = new ArrayList<Book>();
    	Book temp = null;
    	for(int i = 0; i < booksId.length; i++) {
    		temp = bookService.getBookById(booksId[i]);
    		book.add(temp);
    	}
    	Set<Book> books = new HashSet<Book>(book);
    	Date date = new Date();

    	contract.setContractUser(user);
    	contract.setContractCreatedDate(date);
    	contract.setContractPaymentMethod(paymentMethod);
    	contract.setBooks(books);
    	contract.setContractStatus(paymentMethod.equals(PaymentMethod.Cash_Account.value()) ? ContractStatus.Confirmed.value() : ContractStatus.Pending.value());
    	contractService.addContract(contract);

    	if (paymentMethod.equals(PaymentMethod.Cash_Account.value())){

    		// Add book into user shelf
    		double balance = user.getUserAcctBalance();
        	double grandTotal = book.stream().mapToDouble(eachBook -> eachBook.getBookPrice()).sum();

        	if (grandTotal > balance) {
        		System.out.println("Grand total  " + grandTotal + " exceed balance amount " + balance);
        		return new ResponseEntity<Void>(HttpStatus.PAYMENT_REQUIRED);
        	}

        	// Deduct from user account balance
        	user.setUserAcctBalance(balance - grandTotal);

        	// Add CashAccount Transaction
        	CashAcctTrans transaction = new CashAcctTrans();
        	transaction.setCashAcctTransContract(contract);
        	transaction.setCashAcctTransUser(user);
        	transaction.setCastAcctTransAmt(-grandTotal); // negative for deduct
        	transaction.setCashAcctTransCreatedDate(new Date());


        	book.addAll(user.getShelf().getBooks());
        	user.getShelf().setBooks(book);

        	shelfService.updateShelfNumberOfBook(user.getShelf());
        	userService.updateUser(user);
        	cashAccTransService.addCashAcctTrans(transaction);

    	} else { // Pay by Cheque

    		Cheque cheque = new Cheque();
    		cheque.setChequeContract(contract.getContractNumber());
    		cheque.setChequeUser(user);
    		cheque.setChequeCreatedDate(new Date());
    		cheque.setChequeStatus(ChequeStatus.Pending.value());
    		cheque.setChequeAmt(contractForm.getChequeAmount());
    		cheque.setChequeNumber(contractForm.getChequeNumber());

    		contract.setContractStatus(ContractStatus.Pending.value());

    		chequeService.addCheque(cheque);
    	}



/*        if (authorService.isUserExist(author)) {
            System.out.println("A author with name " + user.getUsername() + " already exist");
            return new ResponseEntity<Void>(HttpStatus.CONFLICT);
        }*/

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/contract-user/{contractId}").buildAndExpand(contract.getContractNumber()).toUri());
        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
    }

  //-------------------Void a contract--------------------------------------------------------
    @RequestMapping(value = "/contract-void/{contractNum}", method = RequestMethod.GET)
    public ResponseEntity<Void> voidContract(@PathVariable("contractNum") String contractNum) {
    	System.out.println("Fetching contract with contract number " + contractNum);
    	Contract contract = contractService.getContractById(contractNum);
    	if (contract == null) {
    		System.out.println("Contract with number " + contractNum + " not found");
    		return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
    	}

    	contractService.voidContract(contract);
		return new ResponseEntity<Void>(HttpStatus.OK);

    }

    //-------------------Approve a contract--------------------------------------------------------
    @RequestMapping(value = "/contract-approve/", method = RequestMethod.GET)
    public ResponseEntity<Void> approveContract(@RequestParam(value = "moderatorId") String moderatorId, @RequestParam(value = "contractNum") String contractNum) {
    	System.out.println("Fetching contract with contract number " + contractNum);
    	Contract contract = contractService.getContractById(contractNum);
    	if (contract == null) {
    		System.out.println("Contract with number " + contractNum + " not found");
    		return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
    	}

    	Moderator moderator = moderatorService.getById(moderatorId);
    	if (moderator == null) {
    		System.out.println("Moderator with Id " + moderatorId + " not found");
    		return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
    	}

    	//Get Cheque
    	Cheque cheque = chequeService.getChequeById(contractNum);
    	if (cheque == null) {
    		System.out.println("Cheque with contract number " + contractNum + " not found");
    		return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
    	}

    	// Calculate deduction
    	User user = contract.getContractUser();
    	double balance = user.getUserAcctBalance();
    	double paymentAmount = cheque.getChequeAmt();
    	double grandTotal = contract.getBooks().stream().mapToDouble(eachBook -> eachBook.getBookPrice()).sum();

    	if (grandTotal > paymentAmount) {
    		System.out.println("Grand total  " + grandTotal + " exceed balance amount " + balance);
    		return new ResponseEntity<Void>(HttpStatus.PAYMENT_REQUIRED);
    	}

    	//update cheque
    	cheque.setChequeApprovedBy(moderator);
    	cheque.setChequeApprovedDate(new Date());
    	cheque.setChequeStatus(ChequeStatus.Confirmed.value());

    	// Add CashTransaction IN
    	double transferInAmount = paymentAmount - grandTotal;
    	CashAcctTrans transaction = new CashAcctTrans();
    	transaction.setCashAcctTransContract(contract);
    	transaction.setCashAcctTransUser(user);
    	transaction.setCastAcctTransAmt(transferInAmount);
    	transaction.setCashAcctTransCreatedDate(new Date());

    	// Remaining balance set into user account balance
    	user.setUserAcctBalance(balance + transferInAmount);

    	// Set Approval Moderator
    	contract.setContractApprovedBy(moderator);

    	List<Book> books = contract.getBooks().stream().collect(Collectors.toList());
    	books.addAll(user.getShelf().getBooks());
    	user.getShelf().setBooks(books);

    	chequeService.approveCheque(cheque);
    	cashAccTransService.addCashAcctTrans(transaction);
    	shelfService.updateShelfNumberOfBook(user.getShelf());
    	userService.updateUser(user);
    	contractService.approveContract(contract);
		return new ResponseEntity<Void>(HttpStatus.OK);

    }

  //-------------------Reject a contract--------------------------------------------------------
    @RequestMapping(value = "/contract-reject/", method = RequestMethod.GET)
    public ResponseEntity<Void> rejectContract(@RequestParam(value = "moderatorId") String moderatorId, @RequestParam(value = "contractNum") String contractNum) {
    	System.out.println("Fetching contract with contract number " + contractNum);
    	Contract contract = contractService.getContractById(contractNum);
    	if (contract == null) {
    		System.out.println("Contract with number " + contractNum + " not found");
    		return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
    	}

    	Moderator moderator = moderatorService.getById(moderatorId);
    	if (moderator == null) {
    		System.out.println("Moderator with Id " + moderatorId + " not found");
    		return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
    	}

    	//Get Cheque
    	Cheque cheque = chequeService.getChequeById(contractNum);
    	if (cheque == null) {
    		System.out.println("Cheque with contract number " + contractNum + " not found");
    		return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
    	}

    	//update cheque
    	cheque.setChequeApprovedBy(moderator);
    	cheque.setChequeApprovedDate(new Date());
    	cheque.setChequeStatus(ChequeStatus.Reject.value());

    	// Set Approval Moderator
    	contract.setContractApprovedBy(moderator);
    	contractService.rejectContract(contract);
    	chequeService.updateCheque(cheque);

		return new ResponseEntity<Void>(HttpStatus.OK);

    }


/*    //------------------- Update a book --------------------------------------------------------

    @RequestMapping(value = "/book-moderator/{bookId}", method = RequestMethod.PUT)
    public ResponseEntity<Book> updateBook(@PathVariable("bookId") String bookId, @RequestBody Book book) {
        System.out.println("Updating book " + bookId);

        Book currentBook = bookService.getBookById(bookId);

        if (currentBook==null) {
            System.out.println("book with id " + bookId + " not found");
            return new ResponseEntity<Book>(HttpStatus.NOT_FOUND);
        }

        currentBook.setBookSubject(book.getBookSubject());
        currentBook.setBookDescription(book.getBookDescription());
        currentBook.setBookIsbn(book.getBookIsbn());
        currentBook.setBookAuthor(book.getBookAuthor());
        currentBook.setBookPublisher(book.getBookPublisher());
        currentBook.setBookCategory(book.getBookCategory());
        currentBook.setBookContent(book.getBookContent());
        currentBook.setBookContentType(book.getBookContentType());
        currentBook.setBookPrice(book.getBookPrice());

        bookService.updateBook(currentBook);
        return new ResponseEntity<Book>(currentBook, HttpStatus.OK);
    }

     */

/*    //------------------- Delete a author --------------------------------------------------------

    @RequestMapping(value = "/author/{authorId}", method = RequestMethod.DELETE)
    public ResponseEntity<Author> deleteAuthor(@PathVariable("id") long id) {
        System.out.println("Fetching & Deleting User with id " + id);

        User user = authorService.findById(id);
        if (user == null) {
            System.out.println("Unable to delete. User with id " + id + " not found");
            return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
        }

        authorService.deleteUserById(id);
        return new ResponseEntity<User>(HttpStatus.NO_CONTENT);
    }



    //------------------- Delete All author --------------------------------------------------------

    @RequestMapping(value = "/user/", method = RequestMethod.DELETE)
    public ResponseEntity<User> deleteAllUsers() {
        System.out.println("Deleting All Users");

        authorService.deleteAllUsers();
        return new ResponseEntity<User>(HttpStatus.NO_CONTENT);
    }*/

}