package org.jeff.web.webform;

public class ContractForm {
	private String userId;
	private String paymentMethod;
	private String[] booksId;
	private String chequeNumber;
	private double chequeAmount;

	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getPaymentMethod() {
		return paymentMethod;
	}
	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}
	public String[] getBooksId() {
		return booksId;
	}
	public void setBooksId(String[] booksId) {
		this.booksId = booksId;
	}
	public String getChequeNumber() {
		return chequeNumber;
	}
	public void setChequeNumber(String chequeNumber) {
		this.chequeNumber = chequeNumber;
	}
	public double getChequeAmount() {
		return chequeAmount;
	}
	public void setChequeAmount(double chequeAmount) {
		this.chequeAmount = chequeAmount;
	}

}
