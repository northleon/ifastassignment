package org.jeff.web.webform;

public class UserStatusForm {
	private String moderatorId;
	private String userId;
	private String currentStatus;
	private String changeStatus;
	public String getModeratorId() {
		return moderatorId;
	}
	public void setModeratorId(String moderatorId) {
		this.moderatorId = moderatorId;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getCurrentStatus() {
		return currentStatus;
	}
	public void setCurrentStatus(String currentStatus) {
		this.currentStatus = currentStatus;
	}
	public String getChangeStatus() {
		return changeStatus;
	}
	public void setChangeStatus(String changeStatus) {
		this.changeStatus = changeStatus;
	}
	@Override
	public String toString() {
		return "UserStatusForm [moderatorId=" + moderatorId + ", userId=" + userId + ", currentStatus=" + currentStatus
				+ ", changeStatus=" + changeStatus + "]";
	}


}
