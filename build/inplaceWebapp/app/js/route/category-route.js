'use strict';
(function()
{
	angular
	.module('jeff.route.app')
	.config(['$urlRouterProvider','$stateProvider',function($urlRouterProvider,$stateProvider) {
		$urlRouterProvider.otherwise('/');
		$stateProvider
			.state('category-moderator', {
				url: '/category-moderator',
				views: {
					'header': {templateUrl: "app/js/html/templates/header-moderator.html"},
					'footer': {templateUrl: "app/js/html/templates/footer.html"},
					'content':{templateUrl: "app/js/html/category.html"}
				}	
			})
	}]);

	
})();
