'use strict';
(function()
{
	angular
	.module('jeff.route.app')
	.config(['$urlRouterProvider','$stateProvider',function($urlRouterProvider,$stateProvider) {
		$urlRouterProvider.otherwise('/');
		$stateProvider
			.state('register', {
				url: '/register',
				views: {
					'header': {templateUrl: "app/js/html/templates/header.html"},
					'footer': {templateUrl: "app/js/html/templates/footer.html"},
					'content':{templateUrl: "app/js/html/register.html"}
				}	
			})
	}]);

	
})();
