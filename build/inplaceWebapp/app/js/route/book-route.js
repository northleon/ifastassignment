'use strict';
(function()
{
	angular
	.module('jeff.route.app')
	.config(['$urlRouterProvider','$stateProvider',function($urlRouterProvider,$stateProvider) {
		$urlRouterProvider.otherwise('/');
		$stateProvider
			.state('book', {
				url: '/',
				views: {
					'header': {templateUrl: "app/js/html/templates/header.html"},
					'footer': {templateUrl: "app/js/html/templates/footer.html"},
					'content':{templateUrl: "app/js/html/book-user.html"}
				}	
			})
			.state('book-user', {
				url: '/book-user',
				views: {
					'header': {templateUrl: "app/js/html/templates/header-user.html"},
					'footer': {templateUrl: "app/js/html/templates/footer.html"},
					'content':{templateUrl: "app/js/html/book-user.html"}
				}	
			})
			.state('book-moderator', {
				url: '/book-moderator',
				views: {
					'header': {templateUrl: "app/js/html/templates/header-moderator.html"},
					'footer': {templateUrl: "app/js/html/templates/footer.html"},
					'content':{templateUrl: "app/js/html/book-moderator.html"}
				}	
			})
	}]);

	
})();
