'use strict';
(function()
{
	angular
	.module('jeff.route.app')
	.config(['$urlRouterProvider','$stateProvider',function($urlRouterProvider,$stateProvider) {
		$urlRouterProvider.otherwise('/');
		$stateProvider
			.state('contract-user', {
				url: '/contract-user',
				views: {
					'header': {templateUrl: "app/js/html/templates/header-user.html"},
					'footer': {templateUrl: "app/js/html/templates/footer.html"},
					'content':{templateUrl: "app/js/html/contract-user.html"}
				}	
			})
			.state('contract-moderator', {
				url: '/contract-moderator',
				views: {
					'header': {templateUrl: "app/js/html/templates/header-moderator.html"},
					'footer': {templateUrl: "app/js/html/templates/footer.html"},
					'content':{templateUrl: "app/js/html/cart.html"}
				}	
			})
	}]);

	
})();
