'use strict';
(function()
{
	angular
	.module('jeff.route.app')
	.config(['$urlRouterProvider','$stateProvider',function($urlRouterProvider,$stateProvider) {
		$urlRouterProvider.otherwise('/');
		$stateProvider
			.state('cart', {
				url: '/cart',
				views: {
					'header': {templateUrl: "app/js/html/templates/header-user.html"},
					'footer': {templateUrl: "app/js/html/templates/footer.html"},
					'content':{templateUrl: "app/js/html/cart.html"}
				}	
			})
	}]);

	
})();
