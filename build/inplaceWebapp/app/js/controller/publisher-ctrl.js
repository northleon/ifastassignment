'use strict';
angular
.module('jeff.app')
.controller('PublisherController', ['$scope','PublisherService', function($scope,PublisherService) {
          var self = this;
          self.publisher={publisherId:null,
		        		  publisherName:'',
		        		  publisherEmail:'',
		        		  publisherAddress:'',
		        		  publisherTelephone:'',
		        		  publisherFax:''};
          self.publishers=[];

          self.findAllPublisher = function(){
        	  PublisherService.findAllPublisher()
                  .then(
                               function(d) {
                                    self.publishers = d;
                               },
                                function(errResponse){
                                    console.error('Error while fetching publishers');
                                }
                       );
          };

          self.validatePublisher = (publisher) => {
        	  let emailRegEx = /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
        	  let phoneNoRegEx = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/;

        	  if (publisher.publisherName.length > 50) {
        		  alert('Publisher Name too long');
        		  return false;
        	  }

        	  if (publisher.publisherEmail.length > 30) {
        		  alert('Publisher Email too long');
        		  return false;
        	  }

        	  let validEmail = publisher.publisherEmail.match(emailRegEx);
        	  if (!validEmail) {
        		  alert("Please enter a valid email.")
        		  return false;
        	  }

        	  if (publisher.publisherAddress.length > 100) {
        		  alert('Publisher Address too long');
        		  return false;
        	  }

        	  if (publisher.publisherTelephone.length > 30) {
        		  alert('Publisher Telephone too long');
        		  return false;
        	  }

        	  let validPhoneNo = publisher.publisherTelephone.match(phoneNoRegEx);
        	  if (!validPhoneNo) {
        		  alert('Invalid Publisher Telephone.');
        		  return false;
        	  }

        	  if (publisher.publisherFax.length > 30) {
        		  alert('Publisher Fax too long');
        		  return false;
        	  }

        	  let validFax = publisher.publisherFax.match(phoneNoRegEx);
        	  if (!validFax) {
        		  alert('Invalid Publisher Fax');
        		  return false;
        	  }

        	  return true;
          }

          self.addPublisher = function(publisher){
        	  let valid = self.validatePublisher(publisher);
        	  if (!valid) {
        		  console.log('Publisher validation failed');
        		  return false;
        	  }

        	  PublisherService.addPublisher(publisher)
                      .then(
                      self.findAllPublisher,
                              function(errResponse){
                                   console.error('Error while creating publisher.');
                              }
                  );
          };

         self.updatePublisher = function(publisher, publisherId){
        	 PublisherService.updatePublisher(publisher, publisherId)
                      .then(
                              self.findAllPublisher,
                              function(errResponse){
                                   console.error('Error while updating publisher.');
                              }
                  );
          };

         self.deletePublisher = function(publisherId){
        	 PublisherService.deletePublisher(publisherId)
                      .then(
                              self.findAllPublisher,
                              function(errResponse){
                                   console.error('Error while deleting publisher.');
                              }
                  );
          };

          self.findAllPublisher();

          self.submit = function() {
              if(self.publisher.publisherId===null){
                  console.log('Saving New Publisher', self.publisher);
                  self.addPublisher(self.publisher);
              }else{
                  self.updatePublisher(self.publisher, self.publisher.publisherId);
                  console.log('Publisher updated with publisherId ', self.publisher.publisherId);
              }
              self.reset();
          };

          self.edit = function(publisherId){
              console.log('publisherId to be edited', publisherId);
              for(var i = 0; i < self.publishers.length; i++){
                  if(self.publishers[i].publisherId === publisherId) {
                     self.publisher = angular.copy(self.publishers[i]);
                     break;
                  }
              }
          };

          self.remove = function(publisherId){
              console.log('publisherId to be deleted', publisherId);
              if(self.publisher.publisherId === publisherId) {//clean form if the category to be deleted is shown there.
                 self.reset();
              }
              self.deleteCategory(publisherId);
          };


          self.reset = function(){
        	  self.publisher={publisherId:null,
	        		  publisherName:'',
	        		  publisherEmail:'',
	        		  publisherAddress:'',
	        		  publisherTelephone:'',
	        		  publisherFax:''};
              $scope.formPublisher.$setPristine(); //reset Form
          };

      }]);