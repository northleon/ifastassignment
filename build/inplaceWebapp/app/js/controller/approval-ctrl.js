'use strict';
angular
.module('jeff.app')
.controller('ApprovalController', ['$scope', 'UserService','ContractService', function($scope,UserService,ContractService) {
          var self = this;
          $scope.query = {}
          $scope.queryBy = '$'

          self.contracts = [];


          self.findAllContract = function() {
        	  ContractService.findAllContracts()
        	  	.then(
        	  			function(d) {
        	  				self.contracts = d;
        	  				console.log(self.contracts)
        	  				console.log('userContracts ' + self.contracts.length);
        	  			},
                        function(errResponse){
                            console.error('Error while fetching contracts');
                        }
        	  	)
          };

          self.getGrandTotal = (books) => books.reduce((sum,book) => {
        	  return Number(sum) + Number(book.bookPrice);
    	  }, 0);

          self.approveContract = function(contractNum) {
        	  let moderatorId = window.localStorage['moderatorId'];
        	  ContractService.approveContract(moderatorId, contractNum)
        	  .then(
        			  self.findAllContract,
        			  function(errResponse){
        				  switch(errResponse.status){
            	  			case 402:
            	  				alert('Insufficient payment amount.');
            	  				break;
        				  }
                          console.error('Error while approving contract.');
                     }
        	  )
          }

          self.rejectContract = function(contractNum) {
        	  let moderatorId = window.localStorage['moderatorId'];
        	  ContractService.rejectContract(moderatorId, contractNum)
        	  .then(
        			  self.findAllContract,
        			  function(errResponse){
        				  switch(errResponse.status){
            	  			case 402:
            	  				alert('Insufficient payment amount.');
            	  				break;
        				  }
                          console.error('Error while approving contract.');
                     }
        	  )
          }

          self.findAllContract();
      }]);