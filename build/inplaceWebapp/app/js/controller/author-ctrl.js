'use strict';
angular
.module('jeff.app')
.controller('AuthorController', ['$scope', 'AuthorService', function($scope,AuthorService) {
          var self = this;
          self.author={authorId:null,authorName:'',authorEmail:'',authorCountry:''};
          self.authors=[];

          self.findAllAuthor = function(){
              AuthorService.findAllAuthor()
                  .then(
                               function(d) {
                                    self.authors = d;
                               },
                                function(errResponse){
                                    console.error('Error while fetching authors');
                                }
                       );
          };


          self.validateAuthor = (author) => {
        	  let emailRegEx = /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
        	  let validEmail = author.authorEmail.match(emailRegEx);
        	  if (!validEmail) {
        		  alert("Please enter a valid email.")
        		  return false;
        	  }

        	  if (author.authorName.length > 50) {
        		  alert('Author Name too long');
        		  return false;
        	  }

        	  if (author.authorEmail.length > 30) {
        		  alert('Author Email too long');
        		  return false;
        	  }

        	  if (author.authorCountry > 30) {
        		  alert('Author Country too long');
        		  return false;
        	  }

        	  return true;
          }

          self.addAuthor = function(author){
        	  let valid = self.validateAuthor(author);
        	  if (!valid){
        		  console.log('Author validation failed');
        		  return false;
        	  }

              AuthorService.addAuthor(author)
                      .then(
                      self.findAllAuthor,
                              function(errResponse){
                                   console.error('Error while creating author.');
                              }
                  );
          };

         self.updateAuthor = function(author, authorId){
        	 AuthorService.updateAuthor(author, authorId)
                      .then(
                              self.findAllAuthor,
                              function(errResponse){
                                   console.error('Error while updating author.');
                              }
                  );
          };

         self.deleteAuthor = function(authorId){
              AuthorService.deleteAuthor(authorId)
                      .then(
                              self.findAllAuthor,
                              function(errResponse){
                                   console.error('Error while deleting author.');
                              }
                  );
          };

          self.findAllAuthor();

          self.submit = function() {
              if(self.author.authorId===null){
                  console.log('Saving New Author', self.author);
                  self.addAuthor(self.author);
              }else{
                  self.updateAuthor(self.author, self.author.authorId);
                  console.log('Author updated with authorId ', self.author.authorId);
              }
              self.reset();
          };

          self.edit = function(authorId){
              console.log('authorId to be edited', authorId);
              for(var i = 0; i < self.authors.length; i++){
                  if(self.authors[i].authorId === authorId) {
                     self.author = angular.copy(self.authors[i]);
                     break;
                  }
              }
          };

          self.remove = function(authorId){
              console.log('authorId to be deleted', authorId);
              if(self.auhtor.authorId === authorId) {//clean form if the author to be deleted is shown there.
                 self.reset();
              }
              self.deleteAuthor(authorId);
          };


          self.reset = function(){
        	  self.author={authorId:null,authorName:'',authorEmail:'',authorCountry:''};
              $scope.formAuthor.$setPristine(); //reset Form
          };

      }]);