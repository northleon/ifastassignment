'use strict';
var authorApp = angular.module('jeff.app');
authorApp.controller('UpdateUserController', ['$scope', 'UserService', function($scope, UserService) {
          var self = this;
          self.users=[];

          self.updateUser = {
        		  moderatorId: window.localStorage['moderatorId'],
        		  userId: '',
        		  currentStatus: '',
        		  changeStatus: ''
          }

          self.findAllUser = function(){
              UserService.findAllUser()
                  .then(
                       function(d) {
                            self.users = d;
                       },
                        function(errResponse){
                            console.error('Error while fetching users');
                        }
                  );
          };

          self.updateUserStatus = function(updateUser) {
        	  UserService.updateUserStatus(updateUser)
        	  	.then(
    	  			self.findAllUser,
    	  			function(errResponse) {
    	  				console.error('Error while updating user');
    	  			}
        	  	);
          };

          self.unlock = (user) => {
        	  self.updateUser.userId = user.userId;
        	  self.updateUser.currentStatus = user.userStatus;
        	  self.updateUser.changeStatus = 'confirmed';
        	  self.updateUserStatus(self.updateUser);
          }

          self.confirm = (user) => {
        	  self.updateUser.userId = user.userId;
        	  self.updateUser.currentStatus = user.userStatus;
        	  self.updateUser.changeStatus = 'confirmed';
        	  self.updateUserStatus(self.updateUser);
          }

          self.lock = (user) => {
        	  self.updateUser.userId = user.userId;
        	  self.updateUser.currentStatus = user.userStatus;
        	  self.updateUser.changeStatus = 'locked';
        	  self.updateUserStatus(self.updateUser);
          }

          self.findAllUser();


      }]);