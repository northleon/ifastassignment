'use strict';
var authorApp = angular.module('jeff.app');
authorApp.controller('UserController', ['$scope', 'UserService', function($scope, UserService) {
          var self = this;
          self.user={userId:null,
        		  userUserName:'',
        		  userPw:'',
        		  userName:'',
        		  userEmail:'',
        		  userTelephone:'',
        		  userMobile:'',
        		  userAddress:'',
        		  userStatus:'',
        		  userCreatedDate:''};
//        		  userUpdatedDate:'',
//        		  userApprovedDate:'',
//        		  userApprovedBy:'',
//        		  userNumberOfRetries:'',
//        		  userLastLoginDate:'',
//        		  userAcctBalance:''};
          self.users=[];

          self.findAllUser = function(){
              UserService.findAllUser()
                  .then(
                               function(d) {
                                    self.users = d;
                               },
                                function(errResponse){
                                    console.error('Error while fetching users');
                                }
                       );
          };

          self.validateUser = function(user){
        	  let emailRegEx = /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
        	  let phoneNoRegEx = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/;

        	  if (user.userUserName.length > 20){
        		  alert('Username too long');
        		  return false;
        	  }

        	  if (user.userName.length > 100) {
        		  alert('Name too long');
        		  return false;
        	  }

        	  if (user.userEmail.length > 30){
        		  alert('Email too long');
        		  return false;
        	  }

        	  let validEmail = user.userEmail.match(emailRegEx);
        	  if (!validEmail) {
        		  alert("Please enter a valid email");
        		  return false;
        	  }

        	  if (user.userTelephone.length > 30) {
        		  alert("Telephone too long");
        		  return false;
        	  }

        	  let validTelephone = user.userTelephone.match(phoneNoRegEx);
        	  if (!validTelephone) {
        		  alert("Invalid Telephone")
        		  return false;
        	  }

        	  if (user.userMobile.length > 30) {
        		  alert("Mobile too long");
        		  return false;
        	  }

        	  let validMobile = user.userMobile.match(phoneNoRegEx);
        	  if (!validMobile) {
        		  alert("Invalid Mobile")
        		  return false;
        	  }

        	  if (user.userAddress.length > 100) {
        		  alert("Address too long");
        		  return false;
        	  }

        	  return true;
          }

          self.successAdd = function(){
        	  self.findAllUser();
        	  alert("Successfully register. Please wait moderator to approve your account.");
          }
          self.addUser = function(user){
        	  let valid = self.validateUser(user);
        	  if (!valid) {
        		  console.log('User validation failed');
        		  return false;
        	  }

        	  user.userPw = sha1(user.userPw);
              UserService.addUser(user)
                      .then(
            		  self.successAdd,
                      	function(errResponse){
                    	  switch(errResponse.status){
            	  			case 409:
            	  				alert('Username has been used.');
            	  				break;
        	  			}
                       console.error('Error while creating user');
                              }
                  );
          };

          self.findAllUser();

          self.submit = function() {
              if(self.user.userId===null){
                  console.log('Saving New User', self.user);
                  self.addUser(self.user);
              }
              self.reset();
          };

          self.reset = function(){
        	  self.user={userId:null,
            		  userUserName:'',
            		  userPw:'',
            		  userName:'',
            		  userEmail:'',
            		  userTelephone:'',
            		  userMobile:'',
            		  userAddress:''};
              $scope.regForm.$setPristine(); //reset Form
          };

      }]);