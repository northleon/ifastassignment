'use strict';
angular
.module('jeff.app')
.controller('HeaderController', ['$scope','$state','$location','CartService',function($scope,$state,$location,CartService) {
	var self = this;
	self.cart = CartService.getCart();
	/*if(window.localStorage['userId'] != null)
		self.loginId = window.localStorage['userId'];
	else if(window.localStorage['moderatorId'] != null)
		self.loginId = window.localStorage['moderatorId'];*/
	
	self.loginId = window.localStorage['userId'];
	self.logout = function() {
		CartService.emptyCart();
		window.localStorage['moderatorId'] = null;
		window.localStorage['userId'] = null;
		$state.go('book');
	}
}]);