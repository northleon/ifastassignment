'use strict';
angular
.module('jeff.service.app')
.factory('UserService', ['$http', '$q', function($http, $q){

    return {
	    	findAllUser: function() {
	            return $http.get('http://localhost:8080/leon-spring-assignment/user/')
	                    .then(
	                            function(response){
	                                return response.data;
	                            },
	                            function(errResponse){
	                                console.error('Error while fetching users');
	                                return $q.reject(errResponse);
	                            }
	                    );
	    	},

            addUser: function(user){
                    return $http.post('http://localhost:8080/leon-spring-assignment/user/', user)
                            .then(
                                    function(response){
                                        return response.data;
                                    },
                                    function(errResponse){
                                        console.error('Error while creating user');
                                        return $q.reject(errResponse);
                                    }
                            );
            },

            getUser: function(userId){
                return $http.get('http://localhost:8080/leon-spring-assignment/user/'+userId)
                        .then(
                                function(response){
                                    return response.data;
                                },
                                function(errResponse){
                                    console.error('Error while fetching user.');
                                    return $q.reject(errResponse);
                                }
                        );
            },

            updateUserStatus: function(userStatusForm){
            	let request = {
            		method: 'PUT',
            		url: 'http://localhost:8080/leon-spring-assignment/user/update-status',
					headers : {
						'User-Role' : 'Moderator'
					},
					data: userStatusForm
            	};
            	return $http(request)
            			.then(
            					function(response){
            						return response.data;
            					},
            					function(errResponse){
            						console.error('Error while updating user.');
            						return $q.reject(errResponse);
            					}
    					);
            }

    };

}]);