/*'use strict';
angular
.module('jeff.service.app')
.factory('BookService', ['$http', '$log', '$q', BookService]);

function BookService($http, $log, $q) {
	var ret = {};
	
	ret.findAllBook = function () {
		return $http({
			method : 'GET',
			url : 'http://localhost:8080/leon-spring-assignment/book-list/',
		}).then(
                function(response){
                    return response.data;
                }, 
                function(errResponse){
                    console.error('Error while fetching books');
                    return $q.reject(errResponse);
                }
        );
	};
	
	ret.addBook = function (book) {
		return $http({
			method : 'POST',
			url : 'http://localhost:8080/leon-spring-assignment/book-moderator/',
			params : {
		bookSubject:book.bookSubject,
		  bookDescription:book.bookDescription,
		  bookIsbn:book.bookIsbn,
		  bookAuthor:book.author,
		  bookPublisher:book.publisher,
		  bookCategory:book.category,
		  bookContent:book.bookContent,
		  bookContentType:book.bookContentType,
		  bookPrice:book.bookPrice}
		}).then(
                function(response){
                    return response.data;
                }, 
                function(errResponse){
                    console.error('Error while creating book');
                    return $q.reject(errResponse);
                }
        );
	};
	
	return ret;
}*/

'use strict';
angular
.module('jeff.service.app')
.factory('BookService', ['$http', '$q', function($http, $q){
 
    return {
            findAllBook: function() {
                    return $http.get('http://localhost:8080/leon-spring-assignment/book-list/')
                            .then(
                                    function(response){
                                        return response.data;
                                    }, 
                                    function(errResponse){
                                        console.error('Error while fetching books');
                                        return $q.reject(errResponse);
                                    }
                            );
            },
            
            searchBook: function(criteria, value) {
                return $http.get('http://localhost:8080/leon-spring-assignment/search-book/' ,{params:{criteria: criteria,value: value}})
                        .then(
                                function(response){
                                    return response.data;
                                }, 
                                function(errResponse){
                                    console.error('Error while fetching books');
                                    return $q.reject(errResponse);
                                }
                        );
        },
             
            addBook: function(book){
                    return $http.post('http://localhost:8080/leon-spring-assignment/book-moderator/', book)
                            .then(
                                    function(response){
                                        return response.data;
                                    }, 
                                    function(errResponse){
                                        console.error('Error while creating book');
                                        return $q.reject(errResponse);
                                    }
                            );
            },
             
            updateBook: function(book, bookId){
                    return $http.put('http://localhost:8080/leon-spring-assignment/book-moderator/'+bookId, book)
                            .then(
                                    function(response){
                                        return response.data;
                                    }, 
                                    function(errResponse){
                                        console.error('Error while updating book');
                                        return $q.reject(errResponse);
                                    }
                            );
            },
             
            deleteBook: function(bookId){
                    return $http.delete('http://localhost:8080/leon-spring-assignment/book-moderator/'+bookId)
                            .then(
                                    function(response){
                                        return response.data;
                                    }, 
                                    function(errResponse){
                                        console.error('Error while deleting book');
                                        return $q.reject(errResponse);
                                    }
                            );
            }
         
    };
 
}]);