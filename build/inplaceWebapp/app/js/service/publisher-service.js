'use strict';
angular
.module('jeff.service.app')
.factory('PublisherService', ['$http', '$q', function($http, $q){
 
    return {
            findAllPublisher: function() {
                    return $http.get('http://localhost:8080/leon-spring-assignment/publisher-moderator/')
                            .then(
                                    function(response){
                                        return response.data;
                                    }, 
                                    function(errResponse){
                                        console.error('Error while fetching publishers');
                                        return $q.reject(errResponse);
                                    }
                            );
            },
             
            addPublisher: function(publisher){
                    return $http.post('http://localhost:8080/leon-spring-assignment/publisher-moderator/', publisher)
                            .then(
                                    function(response){
                                        return response.data;
                                    }, 
                                    function(errResponse){
                                        console.error('Error while creating publisher');
                                        return $q.reject(errResponse);
                                    }
                            );
            },
             
            updatePublisher: function(publisher, publisherId){
                    return $http.put('http://localhost:8080/leon-spring-assignment/publisher-moderator/'+publisherId, publisher)
                            .then(
                                    function(response){
                                        return response.data;
                                    }, 
                                    function(errResponse){
                                        console.error('Error while updating publisher');
                                        return $q.reject(errResponse);
                                    }
                            );
            },
             
            deletePublisher: function(publisherId){
                    return $http.delete('http://localhost:8080/leon-spring-assignment/publisher-moderator/'+publisherId)
                            .then(
                                    function(response){
                                        return response.data;
                                    }, 
                                    function(errResponse){
                                        console.error('Error while deleting publisher');
                                        return $q.reject(errResponse);
                                    }
                            );
            }
         
    };
 
}]);