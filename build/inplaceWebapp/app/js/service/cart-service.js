'use strict';
angular
.module('jeff.service.app')
.service('CartService', function(){
	var self = this;
	var isDuplicate;
	self.cart=[];
	
	self.addToCart = function(book) {
		console.log("cartService",book);
		self.isDuplicate = false;
		for(var i = 0; i < self.cart.length; i++){
			if(self.cart[i].bookId == book.bookId) {
				self.isDuplicate = true;
				break;
			}
		}
		if(!self.isDuplicate) {
			self.cart.push(book);		
		}
		else {
			console.error("cartService","Duplicate Book found in cart.");
			alert("Duplicate Book found in cart.");
		}
		
		console.log("cartService",self.cart.length);
	}
	
	self.getCart = function() {
		return self.cart;
	}
	
	self.removeFromCart = function(index) {
		self.cart.splice(index, 1);
	}
	
	self.emptyCart = function() {
		self.cart = [];
	}
});