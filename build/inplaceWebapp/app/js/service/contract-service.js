'use strict';
angular
.module('jeff.service.app')
.factory('ContractService', ['$http', '$q', function($http, $q){

    return {
            findAllContracts: function() {
                    return $http.get('http://localhost:8080/leon-spring-assignment/contract-moderator/')
                            .then(
                                    function(response){
                                        return response.data;
                                    },
                                    function(errResponse){
                                        console.error('Error while fetching contracts');
                                        return $q.reject(errResponse);
                                    }
                            );
            },

            findUserContracts: function(userId) {
                return $http.get('http://localhost:8080/leon-spring-assignment/contract-user/'+userId)
                        .then(
                                function(response){
                                    return response.data;
                                },
                                function(errResponse){
                                    console.error('Error while fetching contracts');
                                    return $q.reject(errResponse);
                                }
                        );
            },

            addContract: function(contractForm){
                    return $http.post('http://localhost:8080/leon-spring-assignment/contract-add/', contractForm)
                            .then(
                                    function(response){
                                    	console.log(response.data)
                                    	console.log("addContract","added");
                                        return response.data;
                                    },
                                    function(errResponse){
                                    	console.log("addContract","failed");
                                        console.error('Error while creating contract.');
                                        return $q.reject(errResponse);
                                    }
                            );
            },

            voidContract: function(contractNum){
            	return $http.get('http://localhost:8080/leon-spring-assignment/contract-void/' + contractNum)
            		.then(
            				function(response){
            					console.log("voidContract","voided")
            					return response.data;
            				},
            				function(errResponse){
                            	console.log("voidContract","failed");
                                console.error('Error while voiding contract.');
                                return $q.reject(errResponse);
                            }
            		);
            },

            approveContract: function(mdId, cNum){
            	return $http({
	            		method : 'GET',
	            		url : 'http://localhost:8080/leon-spring-assignment/contract-approve/',
	            		params : { moderatorId : mdId, contractNum : cNum}
            		}).then(
            				function(response){
            					console.log("approveContract","approved");
            					console.log(response.data)
            					return response.data;
            				},
            				function(errResponse){
            					console.log("approveContract","failed");
                                console.error('Error while approving contract.');
                                return $q.reject(errResponse);
            				}
            		);
            },

            rejectContract: function(mdId, cNum){
            	return $http({
            		method : 'GET',
            		url : 'http://localhost:8080/leon-spring-assignment/contract-reject/',
            		params : { moderatorId : mdId, contractNum : cNum}
            	}).then(
            			function(response){
            				console.log("rejectContract","rejected");
            				return response.data
            			},
            			function(errResponse){
        					console.log("rejectContract","failed");
                            console.error('Error while rejecting contract.');
                            return $q.reject(errResponse);
        				}
            	)
            }



    };

}]);