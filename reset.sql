--------------------------------------------------------
--  File created - Friday-August-23-2019   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table LEON_AUTHOR
--------------------------------------------------------

  CREATE TABLE "FSM"."LEON_AUTHOR" 
   (	"AUTHOR_ID" VARCHAR2(20 BYTE), 
	"AUTHOR_NAME" VARCHAR2(50 BYTE), 
	"AUTHOR_EMAIL" VARCHAR2(30 BYTE), 
	"AUTHOR_COUNTRY" VARCHAR2(30 BYTE)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "FSM" ;
--------------------------------------------------------
--  DDL for Table LEON_BOOK
--------------------------------------------------------

  CREATE TABLE "FSM"."LEON_BOOK" 
   (	"BOOK_ID" VARCHAR2(20 BYTE), 
	"BOOK_SUBJECT" VARCHAR2(20 BYTE), 
	"BOOK_DESCRIPTION" VARCHAR2(20 BYTE), 
	"BOOK_ISBN" VARCHAR2(20 BYTE), 
	"BOOK_CONTENT" VARCHAR2(20 BYTE), 
	"BOOK_CONTENT_TYPE" VARCHAR2(20 BYTE), 
	"BOOK_AUTHOR" VARCHAR2(20 BYTE), 
	"BOOK_PUBLISHER" VARCHAR2(20 BYTE), 
	"BOOK_CATEGORY" VARCHAR2(20 BYTE), 
	"BOOK_CREATED_DATE" DATE, 
	"BOOK_PRICE" NUMBER(38,2)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "FSM" ;
--------------------------------------------------------
--  DDL for Table LEON_CASH_ACCT_TRANS
--------------------------------------------------------

  CREATE TABLE "FSM"."LEON_CASH_ACCT_TRANS" 
   (	"CASH_ACCT_TRANS_CONTRACT" VARCHAR2(30 BYTE), 
	"CASH_ACCT_TRANS_USER" VARCHAR2(20 BYTE), 
	"CASH_ACCT_TRANS_AMT" NUMBER(38,2), 
	"CASH_ACCT_TRANS_REMARKS" VARCHAR2(200 BYTE), 
	"CASH_ACCT_TRANS_CREATED_DATE" DATE
   ) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  TABLESPACE "FSM" ;
--------------------------------------------------------
--  DDL for Table LEON_CATEGORY
--------------------------------------------------------

  CREATE TABLE "FSM"."LEON_CATEGORY" 
   (	"CATEGORY_ID" VARCHAR2(20 BYTE), 
	"CATEGORY_NAME" VARCHAR2(50 BYTE), 
	"CATEGORY_DESCRIPTION" VARCHAR2(100 BYTE)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "FSM" ;
--------------------------------------------------------
--  DDL for Table LEON_CHEQUE
--------------------------------------------------------

  CREATE TABLE "FSM"."LEON_CHEQUE" 
   (	"CHEQUE_CONTRACT" VARCHAR2(30 BYTE), 
	"CHEQUE_USER" VARCHAR2(20 BYTE), 
	"CHEQUE_CREATED_DATE" DATE, 
	"CHEQUE_AMT" NUMBER(38,2), 
	"CHEQUE_STATUS" VARCHAR2(20 BYTE), 
	"CHEQUE_APPROVED_DATE" DATE, 
	"CHEQUE_APPROVED_BY" VARCHAR2(20 BYTE), 
	"CHEQUE_NUMBER" VARCHAR2(30 BYTE)
   ) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  TABLESPACE "FSM" ;
--------------------------------------------------------
--  DDL for Table LEON_CONTRACT
--------------------------------------------------------

  CREATE TABLE "FSM"."LEON_CONTRACT" 
   (	"CONTRACT_NUMBER" VARCHAR2(30 BYTE), 
	"CONTRACT_USER" VARCHAR2(20 BYTE), 
	"CONTRACT_CREATED_DATE" DATE, 
	"CONTRACT_UPDATED_DATE" DATE, 
	"CONTRACT_VOIDED_DATE" DATE, 
	"CONTRACT_STATUS" VARCHAR2(20 BYTE), 
	"CONTRACT_APPROVED_BY" VARCHAR2(20 BYTE), 
	"CONTRACT_PAYMENT_METHOD" VARCHAR2(30 BYTE)
   ) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  TABLESPACE "FSM" ;
--------------------------------------------------------
--  DDL for Table LEON_CONTRACT_BOOK
--------------------------------------------------------

  CREATE TABLE "FSM"."LEON_CONTRACT_BOOK" 
   (	"CONTRACT_BOOK_CONTRACT_NUMBER" VARCHAR2(20 BYTE), 
	"CONTRACT_BOOK_BOOK_ID" VARCHAR2(20 BYTE)
   ) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  TABLESPACE "FSM" ;
--------------------------------------------------------
--  DDL for Table LEON_MODERATOR
--------------------------------------------------------

  CREATE TABLE "FSM"."LEON_MODERATOR" 
   (	"MODERATOR_ID" VARCHAR2(20 BYTE), 
	"MODERATOR_USERNAME" VARCHAR2(30 BYTE), 
	"MODERATOR_PW" VARCHAR2(50 BYTE), 
	"MODERATOR_ENABLE" NUMBER(38,0), 
	"MODERATOR_LAST_LOGIN_DATE" DATE
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "FSM" ;
--------------------------------------------------------
--  DDL for Table LEON_PUBLISHER
--------------------------------------------------------

  CREATE TABLE "FSM"."LEON_PUBLISHER" 
   (	"PUBLISHER_ID" VARCHAR2(20 BYTE), 
	"PUBLISHER_NAME" VARCHAR2(50 BYTE), 
	"PUBLISHER_EMAIL" VARCHAR2(30 BYTE), 
	"PUBLISHER_ADDRESS" VARCHAR2(100 BYTE), 
	"PUBLISHER_TELEPHONE" VARCHAR2(30 BYTE), 
	"PUBLISHER_FAX" VARCHAR2(30 BYTE)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "FSM" ;
--------------------------------------------------------
--  DDL for Table LEON_SHELF
--------------------------------------------------------

  CREATE TABLE "FSM"."LEON_SHELF" 
   (	"SHELF_ID" NUMBER(38,0), 
	"SHELF_USER" VARCHAR2(20 BYTE), 
	"SHELF_NUMBER_OF_BOOK" NUMBER(38,0), 
	"SHELF_LAST_ACCESS_DATE" DATE, 
	"SHELF_NUMBER_OF_ACCESS" NUMBER(38,0)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "FSM" ;
--------------------------------------------------------
--  DDL for Table LEON_SHELF_BOOK
--------------------------------------------------------

  CREATE TABLE "FSM"."LEON_SHELF_BOOK" 
   (	"SHELF_BOOK_USER_ID" NUMBER(38,0), 
	"SHELF_BOOK_BOOK_ID" VARCHAR2(20 BYTE)
   ) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  TABLESPACE "FSM" ;
--------------------------------------------------------
--  DDL for Table LEON_USER
--------------------------------------------------------

  CREATE TABLE "FSM"."LEON_USER" 
   (	"USER_ID" VARCHAR2(20 BYTE), 
	"USER_USERNAME" VARCHAR2(30 BYTE), 
	"USER_PW" VARCHAR2(50 BYTE), 
	"USER_NAME" VARCHAR2(100 BYTE), 
	"USER_EMAIL" VARCHAR2(50 BYTE), 
	"USER_TELEPHONE" VARCHAR2(30 BYTE), 
	"USER_MOBILE" VARCHAR2(30 BYTE), 
	"USER_ADDRESS" VARCHAR2(100 BYTE), 
	"USER_STATUS" VARCHAR2(20 BYTE), 
	"USER_CREATED_DATE" DATE, 
	"USER_UPDATED_DATE" DATE, 
	"USER_APPROVED_DATE" DATE, 
	"USER_APPROVED_BY" VARCHAR2(30 BYTE), 
	"USER_NUMBER_OF_RETRIES" NUMBER(38,0), 
	"USER_LAST_LOGIN_DATE" DATE, 
	"USER_ACCT_BALANCE" NUMBER(38,2)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "FSM" ;
REM INSERTING into FSM.LEON_AUTHOR
SET DEFINE OFF;
Insert into FSM.LEON_AUTHOR (AUTHOR_ID,AUTHOR_NAME,AUTHOR_EMAIL,AUTHOR_COUNTRY) values ('A0001','testA','test@gmail.com','ABC');
REM INSERTING into FSM.LEON_BOOK
SET DEFINE OFF;
Insert into FSM.LEON_BOOK (BOOK_ID,BOOK_SUBJECT,BOOK_DESCRIPTION,BOOK_ISBN,BOOK_CONTENT,BOOK_CONTENT_TYPE,BOOK_AUTHOR,BOOK_PUBLISHER,BOOK_CATEGORY,BOOK_CREATED_DATE,BOOK_PRICE) values ('B0001','Some Book','Some Description','ISBN','Some Content','1','A0001','P0001','T0001',to_date('23-AUG-19','DD-MON-RR'),10);
REM INSERTING into FSM.LEON_CASH_ACCT_TRANS
SET DEFINE OFF;
REM INSERTING into FSM.LEON_CATEGORY
SET DEFINE OFF;
Insert into FSM.LEON_CATEGORY (CATEGORY_ID,CATEGORY_NAME,CATEGORY_DESCRIPTION) values ('T0001','testC','TestC');
REM INSERTING into FSM.LEON_CHEQUE
SET DEFINE OFF;
REM INSERTING into FSM.LEON_CONTRACT
SET DEFINE OFF;
REM INSERTING into FSM.LEON_CONTRACT_BOOK
SET DEFINE OFF;
REM INSERTING into FSM.LEON_MODERATOR
SET DEFINE OFF;
Insert into FSM.LEON_MODERATOR (MODERATOR_ID,MODERATOR_USERNAME,MODERATOR_PW,MODERATOR_ENABLE,MODERATOR_LAST_LOGIN_DATE) values ('M0001','alpha','601f1889667efaebb33b8c12572835da3f027f78',1,to_date('21-AUG-19','DD-MON-RR'));
REM INSERTING into FSM.LEON_PUBLISHER
SET DEFINE OFF;
Insert into FSM.LEON_PUBLISHER (PUBLISHER_ID,PUBLISHER_NAME,PUBLISHER_EMAIL,PUBLISHER_ADDRESS,PUBLISHER_TELEPHONE,PUBLISHER_FAX) values ('P0001','testP','TestP@gmail.com','address P','123321321231','123321321231');
REM INSERTING into FSM.LEON_SHELF
SET DEFINE OFF;
Insert into FSM.LEON_SHELF (SHELF_ID,SHELF_USER,SHELF_NUMBER_OF_BOOK,SHELF_LAST_ACCESS_DATE,SHELF_NUMBER_OF_ACCESS) values (1,'U0001',0,null,0);
REM INSERTING into FSM.LEON_SHELF_BOOK
SET DEFINE OFF;
REM INSERTING into FSM.LEON_USER
SET DEFINE OFF;
Insert into FSM.LEON_USER (USER_ID,USER_USERNAME,USER_PW,USER_NAME,USER_EMAIL,USER_TELEPHONE,USER_MOBILE,USER_ADDRESS,USER_STATUS,USER_CREATED_DATE,USER_UPDATED_DATE,USER_APPROVED_DATE,USER_APPROVED_BY,USER_NUMBER_OF_RETRIES,USER_LAST_LOGIN_DATE,USER_ACCT_BALANCE) values ('U0001','test','601f1889667efaebb33b8c12572835da3f027f78','testsjdfo','sdfsdj@gmail.com','123123123123','123123123123','sdjifosdfji','confirmed',to_date('23-AUG-19','DD-MON-RR'),to_date('23-AUG-19','DD-MON-RR'),to_date('23-AUG-19','DD-MON-RR'),'M0001',0,to_date('23-AUG-19','DD-MON-RR'),0);
--------------------------------------------------------
--  DDL for Index LEON_AUTHOR_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "FSM"."LEON_AUTHOR_PK" ON "FSM"."LEON_AUTHOR" ("AUTHOR_ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "FSM" ;
--------------------------------------------------------
--  DDL for Index LEON_BOOK_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "FSM"."LEON_BOOK_PK" ON "FSM"."LEON_BOOK" ("BOOK_ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "FSM" ;
--------------------------------------------------------
--  DDL for Index LEON_CASH_ACCT_TRANS_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "FSM"."LEON_CASH_ACCT_TRANS_PK" ON "FSM"."LEON_CASH_ACCT_TRANS" ("CASH_ACCT_TRANS_CONTRACT", "CASH_ACCT_TRANS_USER") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 
  TABLESPACE "FSM" ;
--------------------------------------------------------
--  DDL for Index LEON_CATEGORY_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "FSM"."LEON_CATEGORY_PK" ON "FSM"."LEON_CATEGORY" ("CATEGORY_ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "FSM" ;
--------------------------------------------------------
--  DDL for Index LEON_CHEQUE_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "FSM"."LEON_CHEQUE_PK" ON "FSM"."LEON_CHEQUE" ("CHEQUE_CONTRACT") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 
  TABLESPACE "FSM" ;
--------------------------------------------------------
--  DDL for Index LEON_CONTRACT
--------------------------------------------------------

  CREATE UNIQUE INDEX "FSM"."LEON_CONTRACT" ON "FSM"."LEON_CONTRACT" ("CONTRACT_NUMBER") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 
  TABLESPACE "FSM" ;
--------------------------------------------------------
--  DDL for Index LEON_CONTRACT_BOOK_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "FSM"."LEON_CONTRACT_BOOK_PK" ON "FSM"."LEON_CONTRACT_BOOK" ("CONTRACT_BOOK_CONTRACT_NUMBER", "CONTRACT_BOOK_BOOK_ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 
  TABLESPACE "FSM" ;
--------------------------------------------------------
--  DDL for Index LEON_MODERATOR_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "FSM"."LEON_MODERATOR_PK" ON "FSM"."LEON_MODERATOR" ("MODERATOR_ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "FSM" ;
--------------------------------------------------------
--  DDL for Index LEON_PUBLISHER_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "FSM"."LEON_PUBLISHER_PK" ON "FSM"."LEON_PUBLISHER" ("PUBLISHER_ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "FSM" ;
--------------------------------------------------------
--  DDL for Index LEON_SHELF_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "FSM"."LEON_SHELF_PK" ON "FSM"."LEON_SHELF" ("SHELF_ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "FSM" ;
--------------------------------------------------------
--  DDL for Index LEON_SHELF_BOOK_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "FSM"."LEON_SHELF_BOOK_PK" ON "FSM"."LEON_SHELF_BOOK" ("SHELF_BOOK_USER_ID", "SHELF_BOOK_BOOK_ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 
  TABLESPACE "FSM" ;
--------------------------------------------------------
--  DDL for Index LEON_USER_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "FSM"."LEON_USER_PK" ON "FSM"."LEON_USER" ("USER_ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "FSM" ;
--------------------------------------------------------
--  Constraints for Table LEON_AUTHOR
--------------------------------------------------------

  ALTER TABLE "FSM"."LEON_AUTHOR" ADD CONSTRAINT "LEON_AUTHOR_PK" PRIMARY KEY ("AUTHOR_ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "FSM"  ENABLE;
  ALTER TABLE "FSM"."LEON_AUTHOR" MODIFY ("AUTHOR_ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table LEON_BOOK
--------------------------------------------------------

  ALTER TABLE "FSM"."LEON_BOOK" ADD CONSTRAINT "LEON_BOOK_PK" PRIMARY KEY ("BOOK_ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "FSM"  ENABLE;
  ALTER TABLE "FSM"."LEON_BOOK" MODIFY ("BOOK_PUBLISHER" NOT NULL ENABLE);
  ALTER TABLE "FSM"."LEON_BOOK" MODIFY ("BOOK_AUTHOR" NOT NULL ENABLE);
  ALTER TABLE "FSM"."LEON_BOOK" MODIFY ("BOOK_ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table LEON_CASH_ACCT_TRANS
--------------------------------------------------------

  ALTER TABLE "FSM"."LEON_CASH_ACCT_TRANS" ADD CONSTRAINT "LEON_CASH_ACCT_TRANS_PK" PRIMARY KEY ("CASH_ACCT_TRANS_CONTRACT", "CASH_ACCT_TRANS_USER")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 
  TABLESPACE "FSM"  ENABLE;
  ALTER TABLE "FSM"."LEON_CASH_ACCT_TRANS" MODIFY ("CASH_ACCT_TRANS_USER" NOT NULL ENABLE);
  ALTER TABLE "FSM"."LEON_CASH_ACCT_TRANS" MODIFY ("CASH_ACCT_TRANS_CONTRACT" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table LEON_CATEGORY
--------------------------------------------------------

  ALTER TABLE "FSM"."LEON_CATEGORY" ADD CONSTRAINT "LEON_CATEGORY_PK" PRIMARY KEY ("CATEGORY_ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "FSM"  ENABLE;
  ALTER TABLE "FSM"."LEON_CATEGORY" MODIFY ("CATEGORY_ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table LEON_CHEQUE
--------------------------------------------------------

  ALTER TABLE "FSM"."LEON_CHEQUE" ADD CONSTRAINT "LEON_CHEQUE_PK" PRIMARY KEY ("CHEQUE_CONTRACT")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 
  TABLESPACE "FSM"  ENABLE;
  ALTER TABLE "FSM"."LEON_CHEQUE" MODIFY ("CHEQUE_CONTRACT" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table LEON_CONTRACT
--------------------------------------------------------

  ALTER TABLE "FSM"."LEON_CONTRACT" ADD CONSTRAINT "LEON_CONTRACT" PRIMARY KEY ("CONTRACT_NUMBER")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 
  TABLESPACE "FSM"  ENABLE;
  ALTER TABLE "FSM"."LEON_CONTRACT" MODIFY ("CONTRACT_USER" NOT NULL ENABLE);
  ALTER TABLE "FSM"."LEON_CONTRACT" MODIFY ("CONTRACT_NUMBER" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table LEON_CONTRACT_BOOK
--------------------------------------------------------

  ALTER TABLE "FSM"."LEON_CONTRACT_BOOK" ADD CONSTRAINT "LEON_CONTRACT_BOOK_PK" PRIMARY KEY ("CONTRACT_BOOK_CONTRACT_NUMBER", "CONTRACT_BOOK_BOOK_ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 
  TABLESPACE "FSM"  ENABLE;
--------------------------------------------------------
--  Constraints for Table LEON_MODERATOR
--------------------------------------------------------

  ALTER TABLE "FSM"."LEON_MODERATOR" ADD CONSTRAINT "LEON_MODERATOR_PK" PRIMARY KEY ("MODERATOR_ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "FSM"  ENABLE;
--------------------------------------------------------
--  Constraints for Table LEON_PUBLISHER
--------------------------------------------------------

  ALTER TABLE "FSM"."LEON_PUBLISHER" ADD CONSTRAINT "LEON_PUBLISHER_PK" PRIMARY KEY ("PUBLISHER_ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "FSM"  ENABLE;
--------------------------------------------------------
--  Constraints for Table LEON_SHELF
--------------------------------------------------------

  ALTER TABLE "FSM"."LEON_SHELF" ADD CONSTRAINT "LEON_SHELF_PK" PRIMARY KEY ("SHELF_ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "FSM"  ENABLE;
  ALTER TABLE "FSM"."LEON_SHELF" MODIFY ("SHELF_USER" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table LEON_SHELF_BOOK
--------------------------------------------------------

  ALTER TABLE "FSM"."LEON_SHELF_BOOK" MODIFY ("SHELF_BOOK_USER_ID" NOT NULL ENABLE);
  ALTER TABLE "FSM"."LEON_SHELF_BOOK" ADD CONSTRAINT "LEON_SHELF_BOOK_PK" PRIMARY KEY ("SHELF_BOOK_USER_ID", "SHELF_BOOK_BOOK_ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 
  TABLESPACE "FSM"  ENABLE;
  ALTER TABLE "FSM"."LEON_SHELF_BOOK" MODIFY ("SHELF_BOOK_BOOK_ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table LEON_USER
--------------------------------------------------------

  ALTER TABLE "FSM"."LEON_USER" ADD CONSTRAINT "LEON_USER_PK" PRIMARY KEY ("USER_ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "FSM"  ENABLE;
  ALTER TABLE "FSM"."LEON_USER" MODIFY ("USER_ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Ref Constraints for Table LEON_BOOK
--------------------------------------------------------

  ALTER TABLE "FSM"."LEON_BOOK" ADD CONSTRAINT "LEON_BOOK_FK1" FOREIGN KEY ("BOOK_PUBLISHER")
	  REFERENCES "FSM"."LEON_PUBLISHER" ("PUBLISHER_ID") ENABLE;
  ALTER TABLE "FSM"."LEON_BOOK" ADD CONSTRAINT "LEON_BOOK_FK2" FOREIGN KEY ("BOOK_CATEGORY")
	  REFERENCES "FSM"."LEON_CATEGORY" ("CATEGORY_ID") ENABLE;
  ALTER TABLE "FSM"."LEON_BOOK" ADD CONSTRAINT "LEON_BOOK_FK3" FOREIGN KEY ("BOOK_AUTHOR")
	  REFERENCES "FSM"."LEON_AUTHOR" ("AUTHOR_ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table LEON_CASH_ACCT_TRANS
--------------------------------------------------------

  ALTER TABLE "FSM"."LEON_CASH_ACCT_TRANS" ADD CONSTRAINT "LEON_CASH_ACCT_TRANS_FK1" FOREIGN KEY ("CASH_ACCT_TRANS_CONTRACT")
	  REFERENCES "FSM"."LEON_CONTRACT" ("CONTRACT_NUMBER") ENABLE;
  ALTER TABLE "FSM"."LEON_CASH_ACCT_TRANS" ADD CONSTRAINT "LEON_CASH_ACCT_TRANS_FK2" FOREIGN KEY ("CASH_ACCT_TRANS_USER")
	  REFERENCES "FSM"."LEON_USER" ("USER_ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table LEON_CHEQUE
--------------------------------------------------------

  ALTER TABLE "FSM"."LEON_CHEQUE" ADD CONSTRAINT "LEON_CHEQUE_FK1" FOREIGN KEY ("CHEQUE_APPROVED_BY")
	  REFERENCES "FSM"."LEON_MODERATOR" ("MODERATOR_ID") ENABLE;
  ALTER TABLE "FSM"."LEON_CHEQUE" ADD CONSTRAINT "LEON_CHEQUE_FK2" FOREIGN KEY ("CHEQUE_USER")
	  REFERENCES "FSM"."LEON_USER" ("USER_ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table LEON_CONTRACT
--------------------------------------------------------

  ALTER TABLE "FSM"."LEON_CONTRACT" ADD CONSTRAINT "LEON_CONTRACT_FK1" FOREIGN KEY ("CONTRACT_APPROVED_BY")
	  REFERENCES "FSM"."LEON_MODERATOR" ("MODERATOR_ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table LEON_SHELF
--------------------------------------------------------

  ALTER TABLE "FSM"."LEON_SHELF" ADD CONSTRAINT "LEON_SHELF_FK1" FOREIGN KEY ("SHELF_USER")
	  REFERENCES "FSM"."LEON_USER" ("USER_ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table LEON_USER
--------------------------------------------------------

  ALTER TABLE "FSM"."LEON_USER" ADD CONSTRAINT "LEON_USER_FK1" FOREIGN KEY ("USER_APPROVED_BY")
	  REFERENCES "FSM"."LEON_MODERATOR" ("MODERATOR_ID") ENABLE;
